﻿namespace ReadDoc
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMenu));
            this.rKyokaiKenpo = new System.Windows.Forms.RadioButton();
            this.rKosyueisei = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // rKyokaiKenpo
            // 
            this.rKyokaiKenpo.AutoSize = true;
            this.rKyokaiKenpo.Location = new System.Drawing.Point(158, 75);
            this.rKyokaiKenpo.Name = "rKyokaiKenpo";
            this.rKyokaiKenpo.Size = new System.Drawing.Size(90, 20);
            this.rKyokaiKenpo.TabIndex = 0;
            this.rKyokaiKenpo.TabStop = true;
            this.rKyokaiKenpo.Text = "協会健保";
            this.rKyokaiKenpo.UseVisualStyleBackColor = true;
            // 
            // rKosyueisei
            // 
            this.rKosyueisei.AutoSize = true;
            this.rKosyueisei.Location = new System.Drawing.Point(158, 118);
            this.rKosyueisei.Name = "rKosyueisei";
            this.rKosyueisei.Size = new System.Drawing.Size(90, 20);
            this.rKosyueisei.TabIndex = 0;
            this.rKosyueisei.TabStop = true;
            this.rKosyueisei.Text = "公衆衛生";
            this.rKosyueisei.UseVisualStyleBackColor = true;
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(477, 267);
            this.Controls.Add(this.rKosyueisei);
            this.Controls.Add(this.rKyokaiKenpo);
            this.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frmMenu";
            this.Text = "frmMenu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rKyokaiKenpo;
        private System.Windows.Forms.RadioButton rKosyueisei;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReadDoc
{
    public partial class frmInput_forexcel : Form
    {
        clsKKData.ManageInfo clsKKMng = new clsKKData.ManageInfo();
        List<clsKKData.DetailData> lstKKDetail = new List<clsKKData.DetailData>();
        string strBiko = string.Empty;

        public frmInput_forexcel(clsKKData.ManageInfo _clsKKMng,List<clsKKData.DetailData> _lstKKDetail,string _strBiko)
        {
            InitializeComponent();

            clsKKMng = _clsKKMng;
            lstKKDetail = _lstKKDetail;
            strBiko = _strBiko;

            controlEvents();

            InitGrid();
            InitControls();
            setData();

        }

        /// <summary>
        /// コントロールにイベント割り当て
        /// </summary>
        private void controlEvents()
        {
            textBoxKoban.KeyDown += this.KeyDown;
            textBoxBranch.KeyDown += this.KeyDown;
            textBoxHihoName.KeyDown += this.KeyDown;
            textBoxHihoMark.KeyDown += this.KeyDown;
            textBoxHihoNumber.KeyDown += this.KeyDown;
            textBoxCountedDays.KeyDown += this.KeyDown;
            textBoxCreator.KeyDown += this.KeyDown;
            textBoxStart.KeyDown += this.KeyDown;
            textBoxFinish.KeyDown += this.KeyDown;
            textBoxDisease.KeyDown += this.KeyDown;
            textBoxHosName.KeyDown += this.KeyDown;
            textBoxPageForRezept.KeyDown += this.KeyDown;
            textBoxRezeCount.KeyDown += this.KeyDown;
            textBoxPatientBirthday.KeyDown += this.KeyDown;
            textBoxPatientName.KeyDown += this.KeyDown;
            textBoxShibu.KeyDown += this.KeyDown;
            textBoxShinryoYM.KeyDown += this.KeyDown;

            textBoxKoban.Enter += this.Enter;
            textBoxBranch.Enter += this.Enter;
            textBoxHihoName.Enter += this.Enter;
            textBoxHihoMark.Enter += this.Enter;
            textBoxHihoNumber.Enter += this.Enter;
            textBoxCountedDays.Enter += this.Enter;
            textBoxCreator.Enter += this.Enter;
            textBoxStart.Enter += this.Enter;
            textBoxFinish.Enter += this.Enter;
            textBoxDisease.Enter += this.Enter;
            textBoxHosName.Enter += this.Enter;
            textBoxPageForRezept.Enter += this.Enter;
            textBoxRezeCount.Enter += this.Enter;
            textBoxPatientBirthday.Enter += this.Enter;
            textBoxPatientName.Enter += this.Enter;
            textBoxShibu.Enter += this.Enter;
            textBoxShinryoYM.Enter += this.Enter;


            textBoxKoban.Leave += this.Leave;
            textBoxBranch.Leave += this.Leave;
            textBoxHihoName.Leave += this.Leave;
            textBoxHihoMark.Leave += this.Leave;
            textBoxHihoNumber.Leave += this.Leave;
            textBoxCountedDays.Leave += this.Leave;
            textBoxCreator.Leave += this.Leave;
            textBoxStart.Leave += this.Leave;
            textBoxFinish.Leave += this.Leave;
            textBoxDisease.Leave += this.Leave;
            textBoxHosName.Leave += this.Leave;
            textBoxPageForRezept.Leave += this.Leave;
            textBoxRezeCount.Leave += this.Leave;
            textBoxPatientBirthday.Leave += this.Leave;
            textBoxPatientName.Leave += this.Leave;
            textBoxShibu.Leave += this.Leave;
            textBoxShinryoYM.Leave += this.Leave;


        }


        /// <summary>
        /// 詳細グリッド初期化
        /// </summary>
        private void InitGrid()
        {
            dgv_detail.Rows.Clear();

            dgv_detail.Columns.Add("bunrui","診療識別");
            dgv_detail.Columns.Add("koumoku","項目");
            dgv_detail.Columns.Add("tensu","点数");
            dgv_detail.Columns.Add("suryo","数量");
            dgv_detail.Columns.Add("sum","保険点数");
            dgv_detail.Columns.Add("homiso","保み想");

            dgv_detail.Columns["bunrui"].Width = 160;
            dgv_detail.Columns["koumoku"].Width = 240;
            dgv_detail.Columns["tensu"].Width = 80;
            dgv_detail.Columns["suryo"].Width = 70;
            dgv_detail.Columns["sum"].Width = 100;
            dgv_detail.Columns["homiso"].Width = 80;

            dgv_detail.Rows.Add(300);

           
        }


        /// <summary>
        /// テキストボックスクリア
        /// </summary>
        private void InitControls()
        {

            foreach (Control c in splitContainer.Panel1.Controls)
            {
              
                if (c.GetType() == typeof(TextBox))
                {
                    c.Text = string.Empty;
                }
            }

     


        }


        /// <summary>
        /// データのセット
        /// </summary>
        private void setData()
        {
            textBoxKoban.Text = clsKKMng.strKoban;
            textBoxBranch.Text = clsKKMng.strBranchNumber;
            textBoxHihoName.Text = clsKKMng.strHihoName;
            textBoxHihoMark.Text = clsKKMng.strHihoMark;
            textBoxHihoNumber.Text = clsKKMng.strHihoNum;
            textBoxCountedDays.Text = clsKKMng.strCountedDays;
            textBoxCreator.Text = clsKKMng.strCreator;
            textBoxStart.Text = clsKKMng.strStartDate;
            textBoxFinish.Text = clsKKMng.strFinishtDate;
            textBoxDisease.Text = clsKKMng.strDisease;
            textBoxHosName.Text = clsKKMng.strHosName;
            textBoxShibu.Text = clsKKMng.strShibu;
            textBoxPatientBirthday.Text = clsKKMng.strPatientBirthday;
            textBoxPatientName.Text = clsKKMng.strPatientName;
            textBoxPageForRezept.Text = clsKKMng.strPageForRezept;


            for(int r=0;r<lstKKDetail.Count;r++)
            {
                DataGridViewRow dgvr = dgv_detail.Rows[r];
                dgvr.Cells[0].Value = lstKKDetail[r].strShinryoBunrui.ToString();
                dgvr.Cells[1].Value = lstKKDetail[r].strKomoku.ToString();
                dgvr.Cells[2].Value = lstKKDetail[r].strScore.ToString();
                dgvr.Cells[3].Value = lstKKDetail[r].strAmount.ToString();
                dgvr.Cells[4].Value = lstKKDetail[r].strScoreSum.ToString();
                dgvr.Cells[5].Value = lstKKDetail[r].strHMS.ToString();

            }


            textBoxBiko.Text = strBiko;

        }

        private void dgv_detail_KeyDown(object sender, KeyEventArgs e)
        {
            //削除機能
            if(e.KeyCode==Keys.Delete)
            {
                DataGridViewSelectedCellCollection scc = dgv_detail.SelectedCells;
                foreach (DataGridViewCell c in scc) c.Value = string.Empty;
                
            }

            //コピペ機能
            if(e.Control && e.KeyCode == Keys.V)
            {
                string strclip = string.Empty;
                strclip = System.Windows.Forms.Clipboard.GetText();

                string[] strLine = strclip.Split('\n');


                DataGridViewCell curr = dgv_detail.CurrentCell;


                for (int r = 0; r < strLine.Length; r++)
                {
                    string[] strcell = strLine[r].Split('\t');


                    for (int c = 0; c < strcell.Length; c++)
                    {
                        if (dgv_detail.CurrentCell.ColumnIndex + c > dgv_detail.Columns.Count - 1) break;
                        dgv_detail.Rows[dgv_detail.CurrentCell.RowIndex + r].Cells[dgv_detail.CurrentCell.ColumnIndex + c].Value = strcell[c];

                    }

                }
            }
        }

        private void dgv_detail_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if(e.Control is DataGridViewTextBoxEditingControl)
            {
                DataGridViewTextBoxEditingControl tb = (DataGridViewTextBoxEditingControl)e.Control;
                //tb.KeyPress -= new KeyPressEventHandler(dgv_detail_KeyPress);
                //tb.KeyPress += new KeyPressEventHandler(dgv_detail_KeyPress);
            }
        }

       
        public new void KeyDown(object sender,KeyEventArgs e)
        {
            if (e.KeyCode== Keys.Enter) SendKeys.Send("{tab}");
        }

        public new void Enter(object sender,EventArgs e)
        {
            TextBox t=sender as TextBox;
            t.BackColor = Color.GreenYellow;
        }
        public new void Leave(object sender,EventArgs e)
        {
            TextBox t = sender as TextBox;
            t.BackColor = Color.Empty;
        }
  
        private void buttonHide_Click(object sender, EventArgs e)
        {

        }

        private void checkBoxHide_CheckedChanged(object sender, EventArgs e)
        {

            splitContainer.Panel1Collapsed = checkBoxHide.Checked;
        }

        private void textBoxHosName_Enter(object sender, EventArgs e)
        {

        }
    }



}

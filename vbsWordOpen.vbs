option explicit
on error resume next

dim objWord,objDoc
dim args
dim fso
dim strFullFilePath

dim strKimitusei'【機密性３】はVBから取得する
Const wdSeekCurrentPageHeader=9
Const wdAlignParagraphLeft=0

set args=Wscript.Arguments
set fso=CreateObject("scripting.FileSystemObject")

strKimitusei=split(args(0),",")(1)
strFullFilePath=split(args(0),",")(0)

dim strExtension
strExtension=fso.GetExtensionName(strFullFilePath)'ファイルの拡張子を取得

'wordでない場合抜ける
if strExtension <>"docx" then 
    Wscript.Quit
    wscript.sleep 1500
end if


set objWord=CreateObject("word.application")
objWord.visible=false
set objDoc=objWord.Documents.open(strFullFilePath,,false)


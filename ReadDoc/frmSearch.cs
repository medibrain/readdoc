﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Npgsql;


namespace ReadDoc
{
    public partial class frmSearch : Form
    {
        public enum EnmSearchMode
        {
            協会健保,公衆衛生, 学校共済//20191220150757 furukawa 学校共済追加
                           
        }


        public EnmSearchMode SearchMode;
        public string strPath_archive=string.Empty ;
        public string strCommandLine = string.Empty;

        NpgsqlConnection cn;
        NpgsqlCommand cmd;
        NpgsqlDataAdapter da;
        System.Data.DataTable dt = new DataTable();
        System.Data.DataSet dsSetting = new DataSet();

        string strConnectPath ="";
        string strDrive ="";
        string strUserPassword="";

        #region コンストラクタ
        public frmSearch()
        {
            InitializeComponent();


            //バージョン番号
            System.Reflection.Assembly ass;
            ass = System.Reflection.Assembly.GetExecutingAssembly();
            System.Version ver = ass.GetName().Version;
            tssl.Text = $"{ver.Major}.{ver.Minor}.{ver.Build}.{ver.MinorRevision.ToString().Substring(2)}";


#if DEBUG
            btnImp.Visible = true;
            //20210609172534 furukawa st ////////////////////////
            //協会けんぽインポートボタン　デバッグ時のみ表示
            buttonImp.Visible=true;
            //20210609172534 furukawa ed ////////////////////////
#else
            btnImp.Visible = false;
            buttonImp.Visible = false;
#endif

            if (!LoadSetting())
            {
                Application.Exit();
                return;
                
            }

            CreateCommandLine_Connect();
            SearchMode = EnmSearchMode.協会健保;

            //20210502123808 furukawa st ////////////////////////
            //ファイル保管場所がAD対応してない＋初めて開く場所なのでネットワークドライブにした            
            RunBatch();
            //20210502123808 furukawa ed ////////////////////////

            //20210502134128 furukawa st ////////////////////////
            //古すぎるデータは検索対象外(出納さん）            
            dtpVisible.Value = new DateTime(int.Parse(DateTime.Now.AddYears(-4).Year.ToString()),DateTime.Now.Month,1);
            dtpVisible3.Value = new DateTime(int.Parse(DateTime.Now.AddYears(-4).Year.ToString()), DateTime.Now.Month, 1);
            //20210502134128 furukawa ed ////////////////////////
        }
        #endregion

        #region 設定ロード
        private bool LoadSetting()
        {
            try
            {
                dsSetting.ReadXml("settings.xml");
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            
        }
#endregion

#region ネットワークドライブ
        /// <summary>
        /// ネットワークドライブ割当(kaigai_archiveフォルダを見る際にユーザ・パスワードが必要なため、コマンドラインの方が楽)
        /// </summary>
        private void CreateCommandLine_Connect()
        {
            strConnectPath = dsSetting.Tables["setting"].Rows[0]["ConnectPath"].ToString();
            strDrive= dsSetting.Tables["setting"].Rows[0]["NetworkDrive"].ToString();
            strUserPassword=dsSetting.Tables["setting"].Rows[0]["UserPassword"].ToString();

            System.Text.StringBuilder sb = new StringBuilder();

            //sb.Append(strConnectPath);
            //sb.AppendFormat("net use {0} \\\\192.168.112.78\\kaigai_archive {1}",
            //    "h:", "medi@1128 /user:ito_sachiko");

            sb.AppendFormat("net use {0} {1} {2}",
                strDrive, strConnectPath, strUserPassword);



            strCommandLine = sb.ToString();
        }

        /// <summary>
        /// ネットワークドライブ解除
        /// </summary>
        private void CreateCommandLine_DisConnect()
        {
            string strCmdLine = string.Empty;
            //string strCmdLine = dsSetting.Tables["setting"].Rows[0]["commandLineDisConnect"].ToString();
            
            System.Text.StringBuilder sb = new StringBuilder();
            sb.AppendFormat("net use {0} /delete /yes",strDrive);

            //sb.Append(strCmdLine);
            //sb.AppendFormat("net use {0} /delete", "h:");
            strCommandLine = sb.ToString();
        }


        /// <summary>
        /// ネットワークドライブ割当バッチの起動
        /// </summary>
        /// <returns></returns>
        private bool RunBatch()
        {
            string strBatch = "networkdrive.bat";

            //20191212171025 furukawa st ////////////////////////
            //エラーメッセージ出すためtryにした
            
            try
            {
                System.IO.StreamWriter sw =
                    new System.IO.StreamWriter(strBatch, false, System.Text.Encoding.GetEncoding("shift_jis"));

                sw.WriteLine("@echo off");
                sw.WriteLine(strCommandLine);


                sw.Close();

                System.Diagnostics.Process p = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo pi = new System.Diagnostics.ProcessStartInfo(strBatch);
                pi.CreateNoWindow=true;
                pi.UseShellExecute = false;
                p.StartInfo = pi;
                p.Start();

                p.WaitForExit();

               // System.IO.File.Delete(strBatch);

                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            finally
            {
                System.IO.File.Delete(strBatch);
            }
            //20191212171025 furukawa ed ////////////////////////
        }


#endregion

#region DB接続
        /// <summary>
        /// DB接続
        /// </summary>
        /// <returns></returns>
        private bool cnn()
        {
            try
            {
                string strCmdLine = string.Empty;
                // dsSetting.Tables["setting"].Rows[0]["DBConnection_KyokaiKenpo"].ToString();
                //cn = new NpgsqlConnection();
                //cn.ConnectionString = strCmdLine;
                //cn.Open();
                //cmd = new NpgsqlCommand();
                //cmd.Connection = cn;

                switch (SearchMode)
                {
                    case EnmSearchMode.協会健保:
                        strCmdLine = dsSetting.Tables["setting"].Rows[0]["DBConnection_KyokaiKenpo"].ToString();

                        cn = new NpgsqlConnection();
                        cn.ConnectionString = strCmdLine;
                        cn.Open();
                        cmd = new NpgsqlCommand();
                        cmd.Connection = cn;

                        cmd.CommandText = "select m.被保険者番号,m.被保険者記号 from manage m " +
                                 "inner join fileplace p on " +
                                 "m.項番=p.koban";

                        break;
                    case EnmSearchMode.公衆衛生:

                        strCmdLine = dsSetting.Tables["setting"].Rows[0]["DBConnection_KousyuEisei"].ToString();

                        cn = new NpgsqlConnection();
                        cn.ConnectionString = strCmdLine;
                        cn.Open();
                        cmd = new NpgsqlCommand();
                        cmd.Connection = cn;

                        cmd.CommandText = "select koban from fileplace";

                        break;

                    //20191220150913 furukawa st ////////////////////////
                    //学校共済追加
                    
                    case EnmSearchMode.学校共済:
                        strCmdLine = dsSetting.Tables["setting"].Rows[0]["DBConnection_GakkoKyosai"].ToString();

                        cn = new NpgsqlConnection();
                        cn.ConnectionString = strCmdLine;
                        cn.Open();
                        cmd = new NpgsqlCommand();
                        cmd.Connection = cn;
                        //被保番は「公立学校・123459789」型
                        cmd.CommandText = "select trim(replace(split_part(hihonum,'・',2),'　','')) hihonum from tblperson group by hihonum";

                        
                        break;
                        //20191220150913 furukawa ed ////////////////////////

                }

                //20181105091649 furukawa st ////////////////////////
                //コンボの取得条件を管理表とファイルが合致するレコードにする

                //cmd.CommandText = "select m.被保険者番号,m.被保険者記号 from manage m " +
                //                    "inner join fileplace p on " +
                //                    "m.項番=p.koban";
                //cmd.CommandText = "select 被保険者番号,被保険者記号 from manage";

                //20181105091649 furukawa ed ////////////////////////


                return true;
            }
          
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
#endregion


        private void frmSearch_Load(object sender, EventArgs e)
        {
            //接続できない場合は抜ける
            if (!cnn())
            {
                Application.Exit();
                return;
            }

            //if (!RunBatch())
            //{
            //    MessageBox.Show("フォルダ参照失敗");
            //    Application.Exit();
            //    return;
            //}



            da = new NpgsqlDataAdapter();
            

            da.SelectCommand = cmd;
            da.Fill(dt);

            var acs = new AutoCompleteStringCollection();

            for (int r = 0; r < dt.Rows.Count; r++)
            {
                switch (SearchMode)
                {
                    case EnmSearchMode.公衆衛生:
                        acs.Add(dt.Rows[r]["koban"].ToString());
                        break;
                    case EnmSearchMode.協会健保:
                        acs.Add(dt.Rows[r]["被保険者番号"].ToString());
                        break;
                    case EnmSearchMode.学校共済:
                        acs.Add(dt.Rows[r]["hihonum"].ToString());
                        break;
                }
                
                
            }

            switch (SearchMode)
            {
                case EnmSearchMode.公衆衛生:
                    this.txtKENo.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    this.txtKENo.AutoCompleteSource = AutoCompleteSource.CustomSource;
                    this.txtKENo.AutoCompleteCustomSource = acs;
                    break;
                case EnmSearchMode.協会健保:
                    this.txtNo.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    this.txtNo.AutoCompleteSource = AutoCompleteSource.CustomSource;
                    this.txtNo.AutoCompleteCustomSource = acs;
                    break;
                case EnmSearchMode.学校共済:
                    this.txthnum.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    this.txthnum.AutoCompleteSource = AutoCompleteSource.CustomSource;
                    this.txthnum.AutoCompleteCustomSource = acs;
                    setCountry();
                    break;
            }
          
        }

        /// <summary>
        /// 学校共済用診療年月
        /// </summary>
        /// <param name="strHihoNum">被保険者番号</param>
        private void setYM(string strHihoNum)
        {
            string strCmdLine = string.Empty;
            strCmdLine = dsSetting.Tables["setting"].Rows[0]["DBConnection_GakkoKyosai"].ToString();
            
            cn = new NpgsqlConnection();
            cn.ConnectionString = strCmdLine;
            cn.Open();
            cmd = new NpgsqlCommand();
            cmd.Connection = cn;
            cmd.CommandText = $"select ym from tblperson where trim(hihonum) like '%{strHihoNum}%' group by ym order by ym";

            
            cmbYM.DataSource = null;

            DataTable dt = new DataTable();
            NpgsqlDataAdapter da = new NpgsqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(dt);
            DataRow dr = dt.NewRow();
            dr["ym"] = string.Empty;
            dt.Rows.InsertAt(dr,0);

            cmbYM.DataSource = dt;
            cmbYM.DisplayMember = "ym";
        }

        /// <summary>
        /// 学校共済用国リスト
        /// </summary>
        private void setCountry()
        {
            string strCmdLine = string.Empty;
            strCmdLine = dsSetting.Tables["setting"].Rows[0]["DBConnection_GakkoKyosai"].ToString();

            cn = new NpgsqlConnection();
            cn.ConnectionString = strCmdLine;
            cn.Open();
            cmd = new NpgsqlCommand();
            cmd.Connection = cn;

            cmd.CommandText = "select country from tblperson group by country order by country";
            DataTable dt = new DataTable();
            NpgsqlDataAdapter da = new NpgsqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(dt);

            //20220419113919 furukawa st ////////////////////////
            //空欄の行を追加            
            DataRow dr = dt.NewRow();
            dr[0] = "";
            dt.Rows.InsertAt(dr, 0);
            //20220419113919 furukawa ed ////////////////////////


            cmbCountry.DataSource = dt;
            cmbCountry.DisplayMember = "country";


        }

        /// <summary>
        /// 協会健保用
        /// </summary>
        private void setKigo()
        {
            var acs2 = new AutoCompleteStringCollection();

            //入力した番号に合致する記号を追加
            List<DataRow> dr = dt.Select("被保険者番号='" + txtNo.Text + "'").ToList();

            for (int r = 0; r < dr.Count; r++)
            {
                if (!acs2.Contains(dr[r]["被保険者記号"].ToString()))
                {
                    acs2.Add(dr[r]["被保険者記号"].ToString());
                }
            }
                             
            this.cmbKigo.DataSource = acs2;
            
        }


        /// <summary>
        /// 検索ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFind_Click(object sender, EventArgs e)
        {
            //20201022183001 furukawa st ////////////////////////
            //60分程度開きっぱなしで再検索するとエラー落ちみたいなので、あえて切断して再接続にした
            
            cn.Close();
            if (cn.State != ConnectionState.Open) cn.Open();
            //20201022183001 furukawa ed ////////////////////////

            int intFindLimitDate = dtpVisible.Value.Year * 100 + dtpVisible.Value.Month;

            DataGridViewCellStyle cs = new DataGridViewCellStyle();
            cs.Font = new Font("ＭＳ ゴシック", 11);
            StringBuilder sb = new StringBuilder();


            //対象レコードの検索SQL
            switch (SearchMode)
            {
                case EnmSearchMode.協会健保:
                    #region　協会健保

                    if (txtNo.Text == string.Empty) return;


                    sb.AppendLine("select m.項番,m.依頼年月,f.filename as ファイル,");
                    sb.AppendLine("case m.委託区分 when '1' then '翻訳要' when '2' then '翻訳不要' end as 委託区分,");
                    sb.AppendLine("m.外国語,m.被保険者番号,m.被保険者記号,");
                    sb.AppendLine("f.fullpath ");
                    sb.AppendLine("from manage m ");
                    sb.AppendLine("left join fileplace f on ");
                    sb.AppendLine("m.項番=f.koban ");
                    sb.AppendLine($"and m.依頼年月西暦>={intFindLimitDate} ");


                    //sb.AppendLine("where (f.filename like '%.xlsx' or f.filename like '%.docx' )");



                    if (chkWord.Checked && chkExcel.Checked)
                        //20210121154924 furukawa st ////////////////////////
                        //xlsも行けるように

                        sb.AppendLine("where (f.filename like '%.xls' or f.filename like '%.xlsx' or f.filename like '%.docx' )");
                        //            sb.AppendLine("where (f.filename like '%.xlsx' or f.filename like '%.docx' )");
                    //20210121154924 furukawa ed ////////////////////////


                    else if (chkExcel.Checked)
                        //20210121154924 furukawa st ////////////////////////
                        //xlsも行けるように

                        sb.AppendLine("where (f.filename like '%.xls' or f.filename like '%.xlsx')");
                                //              sb.AppendLine("where (f.filename like '%.xlsx')");
                    //20210121154924 furukawa ed ////////////////////////

                    else if (chkWord.Checked) sb.AppendLine("where (f.filename like '%.docx' )");


                    if (!chkWord.Checked && !chkExcel.Checked)
                    {
                        MessageBox.Show("ExcelかWordをチェックしてください");
                        return;
                    }

                    if (txtNo.Text != string.Empty) sb.AppendFormat(" and m.被保険者番号='{0}' ", txtNo.Text.Trim());
                    if (cmbKigo.Text != string.Empty) sb.AppendFormat(" and m.被保険者記号='{0}'", cmbKigo.Text.Trim());
                    //if (txtCountry.Text != string.Empty) sb.AppendFormat(" and m.国名 like '%{0}%'", txtCountry.Text.Trim());
                    break;

                #endregion

                case EnmSearchMode.公衆衛生:
                    #region 公衆衛生

                    if (txtKENo.Text == string.Empty) return;

                    sb.AppendLine("select fullpath, koban,substr(shortpath,12,6) as 納品年月, filename, shortpath ");
                    sb.AppendLine("from fileplace f  ");

                    if (txtKENo.Text != string.Empty) sb.AppendFormat(" where f.koban='{0}' ", txtKENo.Text.Trim());

                    //20210121155208 furukawa st ////////////////////////
                    //xlsも行けるように
                    
                    sb.AppendLine(" and ((f.filename like '%.xlsx' or f.filename like '%.xls' ");
                    //          sb.AppendLine(" and ((f.filename like '%.xlsx' ");
                    //20210121155208 furukawa ed ////////////////////////


                    if (chkPDF.Checked)
                        sb.AppendLine(" or (f.filename like '%.pdf' )");

                    if (chkWordKE.Checked)
                        sb.AppendLine(" or (f.filename like '%.docx' )");

                    sb.AppendLine("))");
                    sb.AppendLine(" order by substr(shortpath,12,6)");

                    break;
                #endregion

                case EnmSearchMode.学校共済:
                    #region　学校共済

                    if (txthnum.Text == string.Empty && cmbCountry.Text == string.Empty && mtxtBirth.Text.Replace("-", "").Trim() == string.Empty) return;

                    sb.AppendLine("select p.personalinfocd id,p.sibu 支部,p.hihonum 被保険者番号,hihoname 被保険者氏名,");
                    sb.AppendLine("p.juryoname 受療者氏名 ,p.juryobirthday 受療者生年月日,p.ym 診療年月,country 医療機関所属国,f.fullpath ");
                    sb.AppendLine("from tblperson p ");
                    sb.AppendLine("inner join tblfile f on p.filecd=f.filecd ");
                    sb.AppendLine("inner join tbldata d on p.filecd=d.filecd ");


                    sb.AppendLine("where 1=1 ");
                    if (txthnum.Text != string.Empty) sb.AppendFormat(" and p.hihonum like '%{0}%' ", txthnum.Text.Trim());
                    if (mtxtBirth.Text.Replace("-", "").Trim() != string.Empty) sb.AppendFormat(" and p.juryobirthday = '{0}' ", mtxtBirth.Text.Trim());
                    if (cmbCountry.Text != string.Empty) sb.AppendFormat(" and p.country like '%{0}%' ", cmbCountry.Text.Trim());
                    if (cmbYM.Text != string.Empty) sb.AppendFormat(" and p.ym ='{0}' ", cmbYM.Text.Trim());


                    sb.AppendLine("group by ");
                    sb.AppendLine("p.personalinfocd,p.sibu,p.hihonum,p.hihoname," +
                        "p.juryoname,p.juryobirthday,p.ym,p.country,f.fullpath ");

                    sb.AppendLine("order by ");
                    sb.AppendLine("p.personalinfocd ");

                    break;
                    #endregion

            }

            cmd.CommandText = sb.ToString();

            DataTable dtt = new DataTable();
            da.SelectCommand = cmd;

            da.Fill(dtt);


            DataGridViewButtonColumn bc = new DataGridViewButtonColumn();
            bc.Width = 50;
            bc.Text = "閲覧";
            bc.HeaderText = "閲覧";
            bc.UseColumnTextForButtonValue = true;
            bc.Name = "閲覧";
            bc.DefaultCellStyle.BackColor = SystemColors.Control;


            //DataGridViewTextBoxColumn txc = new DataGridViewTextBoxColumn();
            //txc.Width = 50;           
            //txc.HeaderText = "納品年月";            
            //txc.Name = "納品年月";
            //txc.DefaultCellStyle.BackColor = SystemColors.Control;


            if (!dgv.Columns.Contains(bc.Name)) dgv.Columns.Add(bc);
                        
            dgv.DataSource = dtt;
            
            dgv.Columns["fullpath"].Visible = false;
            dgv.DefaultCellStyle = cs;
            dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGreen;

        }

        private void txt_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) SendKeys.Send("{tab}");

        }

        private void txt_Validated(object sender, EventArgs e)
        {
            setKigo();
        }

        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //ボタン列押したとき
            if (e.ColumnIndex == 0)
            {
                try
                {
                    //フルパスのネットワークドライブ部分を置き換える
                    string strFilePathOnNetworkDrive =
                        this.dgv.Rows[e.RowIndex].Cells["fullpath"].Value.ToString().Replace(strConnectPath, strDrive);

                   

                    //frmDisp frm = new frmDisp(strFilePathOnNetworkDrive);

                    switch (SearchMode)
                    {
                        case EnmSearchMode.協会健保:
#region 協会健保

                            switch (System.IO.Path.GetExtension(strFilePathOnNetworkDrive))
                            {
                                
                                case ".xlsx":


                                    //フルパスをそのまま使う

                                    //20210310143531 furukawa st ////////////////////////
                                    //項番を引き継いで、DBから取れるように

                                    DataTable dtKK = new DataTable();
                                    cmd.CommandText = $"select * from manage where 項番='{this.dgv.Rows[e.RowIndex].Cells["項番"].Value.ToString()}'";
                                    Npgsql.NpgsqlDataAdapter da = new NpgsqlDataAdapter();
                                    da.SelectCommand = cmd;
                                    da.Fill(dtKK);



                                    frmDisp frm = new frmDisp(this.dgv.Rows[e.RowIndex].Cells["fullpath"].Value.ToString(), dtKK);
                                    //      frmDisp frm = new frmDisp(this.dgv.Rows[e.RowIndex].Cells["fullpath"].Value.ToString());
                                    //20210310143531 furukawa ed ////////////////////////

                                    frm.Show();
                                    break;

                                case ".docx":
                                    //フルパスをそのまま使う
                                    frmDispDoc frm2 = new frmDispDoc(this.dgv.Rows[e.RowIndex].Cells["fullpath"].Value.ToString());
                                    //frm2.Show();
                                    break;

                            }
#endregion
                            break;

                        case EnmSearchMode.学校共済:
#region 学校共済
                             
                            switch (System.IO.Path.GetExtension(strFilePathOnNetworkDrive))
                            {

                                case ".xlsx":
                                    System.Text.StringBuilder sb = new StringBuilder();
                                    DataSet tmpds = new DataSet();
                                    DataTable tmpdtData = new DataTable();
                                    DataTable tmpdtPersonal = new DataTable();
                                    Npgsql.NpgsqlDataAdapter tmpda = new NpgsqlDataAdapter();

                                    //データ部
                                    sb.Remove(0, sb.ToString().Length);
                                    sb.AppendLine(" select ");
                                    sb.AppendLine(" d.rowno,d.bunrui,d.komoku,d.tensu,d.suryo,d.gokei  ");
                                    sb.AppendLine(" from tbldata d inner join tblfile f on ");
                                    sb.AppendLine(" d.filecd=f.filecd ");

                                    //20220413171946 furukawa st ////////////////////////
                                    //学校共済　filecd重複対応結合条件追加

                                    sb.AppendLine("  and d.personalinfocd=f.datakey ");
                                    //20220413171946 furukawa ed ////////////////////////

                                    sb.AppendFormat(" where f.fullpath='{0}' ",this.dgv.Rows[e.RowIndex].Cells["fullpath"].Value.ToString());

                                    //20210609171224 furukawa st ////////////////////////
                                    //学校共済は行番号でソート
                                    
                                    sb.AppendLine(" order by d.rowno ");
                                    //20210609171224 furukawa ed ////////////////////////


                                    cmd.CommandText = sb.ToString();
                                    
                                    tmpda.SelectCommand = cmd;
                                    tmpda.Fill(tmpdtData);
                                    tmpdtData.TableName = "data";
                                    tmpds.Tables.Add(tmpdtData);


                                    //個人情報部
                                    sb.Remove(0, sb.ToString().Length);
                                    sb.AppendLine(" select ");
                                    sb.AppendLine(
                                        "p.sibu," +
                                        "p.hihonum," +
                                        "p.hihoname," +
                                        "p.juryoname," +
                                        "p.juryobirthday," +
                                        "p.ym," +
                                        "p.country," +
                                        "p.dname," +
                                        "p.countday " 
                                        );

                                    sb.AppendLine(" from tblperson p inner join tblfile f on ");
                                    sb.AppendLine(" p.filecd=f.filecd ");

                                    //20220413172030 furukawa st ////////////////////////
                                    //学校共済　filecd重複対応結合条件追加

                                    sb.AppendLine(" and p.personalinfocd=f.datakey ");
                                    //20220413172030 furukawa ed ////////////////////////

                                    sb.AppendFormat(" where f.fullpath='{0}' ", this.dgv.Rows[e.RowIndex].Cells["fullpath"].Value.ToString());
                                    sb.AppendLine("group by p.sibu,p.hihonum,p.hihoname,p.juryoname,p.juryobirthday,p.ym,p.country,p.dname,p.countday ");

                                    cmd.CommandText = sb.ToString();

                                    tmpda.SelectCommand = cmd;
                                    tmpda.Fill(tmpdtPersonal);
                                    tmpdtPersonal.TableName = "personal";
                                    tmpds.Tables.Add(tmpdtPersonal);

                                    if (tmpds.Tables["data"]== null) return;

                                    //表示画面
                                    frmDispGrid frm = new frmDispGrid(tmpds, this.dgv.Rows[e.RowIndex].Cells["fullpath"].Value.ToString());
                                    
                                    //データグリッドで別画面を作成する
                                    //フルパスをそのまま使う
                                    
                                    frm.Show();
                                    break;

                            }
#endregion
                            break;

                        case EnmSearchMode.公衆衛生:
#region 公衆衛生

                            switch (System.IO.Path.GetExtension(strFilePathOnNetworkDrive))
                            {

                                case ".xlsx":
                                case ".xls":

                                    //スクリプト利用のコマンドラインを使うと、ファイルパスに文字化けが発生し、それをロードして失敗する

                                    //System.Diagnostics.ProcessStartInfo pi = new System.Diagnostics.ProcessStartInfo("wscript.exe");
                                    //pi.Arguments = "vbsExcelOpen.vbs " + this.dgv.Rows[e.RowIndex].Cells["fullpath"].Value.ToString();

                                    //System.Diagnostics.Process p = new System.Diagnostics.Process();
                                    //p.StartInfo = pi;
                                    //p.Start();


                                    System.Diagnostics.Process.Start(this.dgv.Rows[e.RowIndex].Cells["fullpath"].Value.ToString());

                                    break;

                                case ".pdf":
                                case ".PDF":

                                    //20191212131429 furukawa st ////////////////////////
                                    //pdfは関連付けからそのまま開かせる
                                    
                                    System.Diagnostics.Process.Start(this.dgv.Rows[e.RowIndex].Cells["fullpath"].Value.ToString());

                                                        //System.Diagnostics.ProcessStartInfo ppi = new System.Diagnostics.ProcessStartInfo("wscript.exe");
                                                        //ppi.Arguments = "vbsPDFOpen.vbs " + this.dgv.Rows[e.RowIndex].Cells["fullpath"].Value.ToString();

                                                        //System.Diagnostics.Process pp = new System.Diagnostics.Process();
                                                        //pp.StartInfo = ppi;
                                                        //pp.Start();


                                    //20191212131429 furukawa ed ////////////////////////

                                    break;
                                case ".docx":
                                    //フルパスをそのまま使う
                                    frmDispDoc frm2 = new frmDispDoc(this.dgv.Rows[e.RowIndex].Cells["fullpath"].Value.ToString());
                                    //frm2.Show();
                                    break;
                                    
                             


                            }

#endregion
                            break;
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
        }

    

        private void cmbKigo_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnFind_Click(sender, e);

        }

        private void frmSearch_FormClosed(object sender, FormClosedEventArgs e)
        {
            CreateCommandLine_DisConnect();
            if (!RunBatch()) return;
            //RunBatch();
        }

        private void selectTab_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (selectTab.SelectedIndex)
            {
                case (int)EnmSearchMode.公衆衛生:
                    SearchMode = EnmSearchMode.公衆衛生;
                    break;
                case (int)EnmSearchMode.協会健保:
                    SearchMode = EnmSearchMode.協会健保;
                    break;
                case (int)EnmSearchMode.学校共済:
                    SearchMode = EnmSearchMode.学校共済;
                    break;
            }
            frmSearch_Load(sender, e);


        }

     
        private void txtKENo_KeyUp(object sender, KeyEventArgs e)
        {
            if (txtKENo.Text.Length > 4) btnFind_Click(sender, e);
        }


        #region 学校共済用取り込み
        //2019年12月16日
        //学校共済用取込モジュール↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
        //1.取込対象のファイルがあるフォルダを指定（学校共済の納品フォルダを、アーカイブにコピーしたやつ）
        //2.県番号取得、管理番号作成
        //3.エクセルの中身を取得
        //4．db登録

        System.IO.StreamWriter swForImport;
        int exceldatano = 0;//レセ単位の連番
        DataTable dtPref = new DataTable();//県番号用

        //db接続
        private Npgsql.NpgsqlConnection conn()
        {
            Npgsql.NpgsqlConnection cn = new NpgsqlConnection();
            
            cn.ConnectionString = dsSetting.Tables["setting"].Rows[0]["DBConnection_GakkoKyosai"].ToString();
            try
            {
                if (cn.State == ConnectionState.Open) cn.Close();
                cn.Open();
            }
            catch(Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + ex.Message);
                return null;
            }
            return cn;
            
        }


        /// <summary>
        /// インポートボタンイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnImp_Click(object sender, EventArgs e)
        {

            //フォルダブラウザの代わりにファイルオープンダイアログ改造版があった
            ReadDoc.Utils.DialogUtility.OpenFolderDialog d = new Utils.DialogUtility.OpenFolderDialog();

            string strpath = string.Empty;
            d.ShowDialog();
            strpath = d.FolderPath;

            if (strpath == string.Empty) return;

            if (!System.IO.Directory.Exists(strPathLog)) System.IO.Directory.CreateDirectory(strPathLog);
            if (!System.IO.Directory.Exists(strPathBlackList)) System.IO.Directory.CreateDirectory(strPathBlackList);

            swForImport = new System.IO.StreamWriter($"{strPathLog}\\{DateTime.Now.ToString("yyyyMMdd_HHmmss")}log.csv", false, System.Text.Encoding.GetEncoding("utf-8"));

            DataTable dtBlack = new DataTable();
            dtBlack.Columns.Add("datakey");
            dtBlack.Columns.Add("reason");

            //db登録用リスト
            DataTable dtForImport = new DataTable();

            dtForImport.Columns.Add("fullpath");
            dtForImport.Columns.Add("modifypath");
            dtForImport.Columns.Add("datano");
            dtForImport.Columns.Add("datakey");
            dtForImport.Columns.Add("personaldata");
            dtForImport.Columns.Add("rowno");
            dtForImport.Columns.Add("rowdata");
            dtForImport.Columns.Add("createdate");
            dtForImport.Columns.Add("updatedate");

            

            if (!CreatePrefList())
            {
                System.Windows.Forms.MessageBox.Show("県番号リスト作成失敗");
                return;
            }

            //管理番号用連番取得
            getExcelDataNo();

            //Excelファイルを取得
            getexcel(strpath, dtForImport);



            //dbへの登録
            lbl.Text = "Excelインポート";
            if (!entry(dtForImport))
            {
                System.Windows.Forms.MessageBox.Show("NG");
            }
            else
            {

                checkPersonalData(dtBlack);
                if (dtBlack.Rows.Count > 0) CreateBlackListFile(dtBlack);

                NpgsqlTransaction tran;
                tran = conn().BeginTransaction();

                lbl.Text = "個人情報部登録";
                Application.DoEvents();

                //個人情報部登録                
                if (!entryPersonalData(tran, dtBlack))
                {
                    tran.Rollback();
                    swForImport.Close();
                    MessageBox.Show("個人情報部登録失敗");
                    return;
                }

                lbl.Text = "明細部登録";
                Application.DoEvents();

                //明細部登録
                if (!entryRowData(tran))
                {
                    tran.Rollback();
                    swForImport.Close();
                    MessageBox.Show("明細部登録登録失敗");//明細部登録
                    return;
                };

                lbl.Text = "ファイル情報";
                Application.DoEvents();


                //ファイル情報
                if (!entryFileInfo(tran))
                {
                    tran.Rollback();
                    swForImport.Close();
                    MessageBox.Show("明細部登録登録失敗");
                    return;
                };


                lbl.Text = "診療年月更新";
                Application.DoEvents();

                //診療年月更新（yyyy/mm）
                if (!UpdateYM(tran))
                {
                    tran.Rollback();
                    swForImport.Close();
                    MessageBox.Show("診療年月更新失敗");
                    return;
                }


                lbl.Text = "インポート日更新";
                Application.DoEvents();

                //インポート日更新
                if (!UpdateImportDate(tran))
                {
                    tran.Rollback();
                    swForImport.Close();
                    MessageBox.Show("インポート日更新失敗");
                    return;
                }


                tran.Commit();

                System.Windows.Forms.MessageBox.Show("取り込みOK");
            }

            swForImport.Close();

            //loadexcel(strpath,no);
        }

        /// <summary>
        /// データ番号の最大を取得
        /// </summary>
        private void getExcelDataNo()
        {
            Npgsql.NpgsqlCommand cmd = new NpgsqlCommand();
            try
            {
                cmd.Connection = conn();
                cmd.CommandText = "select max(cast(filecd as int))+1 from tblfile";
                int.TryParse(cmd.ExecuteScalar().ToString(), out exceldatano);
            }
            catch (Exception ex)
            {
                MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + ex.Message);
            }
            finally
            {
                cmd.Dispose();//AWSコネクション制限対策
            }
        }

        /// <summary>
        /// インポートテーブルにexcelデータを登録
        /// </summary>
        /// <param name="lst">インポート用DataTble</param>
        /// <returns></returns>
        private bool entry(DataTable lst)
        {
          
            Npgsql.NpgsqlCommand cmd = new NpgsqlCommand();
            Npgsql.NpgsqlTransaction tran = conn().BeginTransaction();
            
            cmd.Connection = conn();
            cmd.Transaction = tran;
            
            int r = 0;
            
            try
            {
                for(r=0;r<lst.Rows.Count;r++)                
                {
                    System.Text.StringBuilder sb = new StringBuilder();
                
                    sb.Remove(0, sb.ToString().Length);
                    sb.AppendFormat("insert into tblimport values('{0}','{1}',{2},'{3}','{4}',{5},'{6}','{7}','{8}',null)",
                        lst.Rows[r][0].ToString(),          //0fullpath		
                        lst.Rows[r][1].ToString(),          //1modifypath	
                        lst.Rows[r][2].ToString(),          //2datano	                                                                                   	
                        lst.Rows[r][3].ToString(),          //3datakey	              
                        lst.Rows[r][4].ToString(),          //4personaldata		
                        lst.Rows[r][5].ToString(),          //5rowno		                        	
                        lst.Rows[r][6].ToString(),          //6rowdata		
                        lst.Rows[r][7].ToString(),          //7createdate 	
                        lst.Rows[r][8].ToString()           //8updatedate 	
                        
                        );
                    try
                    {
                        cmd.CommandText = sb.ToString();
                        cmd.ExecuteNonQuery();

                        //進捗表示
                        if (r % 300 == 0)
                        {
                            lbl.Text = $"インポート{r}行";
                            Application.DoEvents();
                        }
                    }
                    catch (NpgsqlException nex)
                    {
                        swForImport.WriteLine("entry:" + lst.Rows[r][0].ToString() + "\n" + nex.Message);
                    }
                    catch (Exception ex)
                    {
                        swForImport.WriteLine("entry:" + lst.Rows[r][0].ToString() + ex.Message);
                    }

                }

                tran.Commit();
                return true;
            }
            catch(NpgsqlException nex)
            {
                swForImport.WriteLine("entry:" + lst.Rows[r][0].ToString() + "\n" + nex.Message);
                return false;
            }
            catch(Exception ex)
            {
                tran.Rollback();
                MessageBox.Show("entry:" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();//AWSコネクション制限対策
                conn().Close();                
            }

            
        }


        /// <summary>
        /// 個人情報部チェック。エラー時はデータキーと共に出力
        /// </summary>
        private void checkPersonalData(DataTable dtBlack)
        {
            Npgsql.NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.Connection = conn();
            DataTable chkdt = new DataTable();
            int r = 0;
            try
            {

                System.Text.StringBuilder sb = new StringBuilder();

                sb.Remove(0, sb.ToString().Length);
                sb.AppendLine("  select ");
                sb.AppendLine(" datakey , ");                                          //personalinfocd
                sb.AppendLine(" split_part(personaldata,',',12) sibu, ");              //支部
                sb.AppendLine(" split_part(personaldata,',',17) hnum, ");              //被保番
                sb.AppendLine(" split_part(personaldata,',',22) hname, ");             //被保険者名
                sb.AppendLine(" split_part(personaldata,',',27) pname, ");             //受療者
                sb.AppendLine(" split_part(personaldata,',',32) pbirth,");             //受療者生年月日
                sb.AppendLine(" split_part(personaldata,',',19) ym,");                 //診療年月
                sb.AppendLine(" split_part(personaldata,',',34) country, ");           //国名
                sb.AppendLine(" split_part(personaldata,',',42) dname, ");             //傷病名
                sb.AppendLine(" split_part(personaldata,',',50) countdays, ");         //診療実日数
                sb.AppendLine(" datano, ");                                            //ファイル連番
                sb.AppendLine(" ''  ");                                                //予備

                sb.AppendLine(" from tblimport ");
                sb.AppendLine(" where importdate is null ");
                sb.AppendLine(" group by  ");
                sb.AppendLine(" datakey , ");                                     //personalinfocd
                sb.AppendLine(" split_part(personaldata,',',12), ");              //支部
                sb.AppendLine(" split_part(personaldata,',',17), ");              //被保番
                sb.AppendLine(" split_part(personaldata,',',22), ");              //被保険者名
                sb.AppendLine(" split_part(personaldata,',',27), ");              //受療者
                sb.AppendLine(" split_part(personaldata,',',32), ");              //受療者生年月日
                sb.AppendLine(" split_part(personaldata,',',19), ");              //診療年月
                sb.AppendLine(" split_part(personaldata,',',34), ");              //国名
                sb.AppendLine(" split_part(personaldata,',',42), ");              //傷病名
                sb.AppendLine(" split_part(personaldata,',',50), ");              //診療実日数
                sb.AppendLine(" datano");                                         //ファイル連番
                                                                                  

                cmd.CommandText=sb.ToString();
                Npgsql.NpgsqlDataAdapter da = new NpgsqlDataAdapter(cmd);
                da.Fill(chkdt);

                try
                {
                    for (r = 0; r < chkdt.Rows.Count; r++)
                    {
                        //ない場合エラー
                        if (chkdt.Rows[r]["datakey"].ToString() == string.Empty)
                        {
                            DataRow drBlack = dtBlack.NewRow();
                            drBlack["datakey"] = "";
                            drBlack["reason"] = "datakeyなし";
                            dtBlack.Rows.Add(drBlack);
                        }

                        //ない場合エラー
                        if (chkdt.Rows[r]["sibu"].ToString() == string.Empty)
                        {
                            DataRow drBlack = dtBlack.NewRow();
                            drBlack["datakey"] = chkdt.Rows[r]["datakey"].ToString();
                            drBlack["reason"] = "支部名なし";
                            dtBlack.Rows.Add(drBlack);
                        }

                        //ない場合エラー
                        if (chkdt.Rows[r]["hnum"].ToString() == string.Empty)
                        {
                            DataRow drBlack = dtBlack.NewRow();
                            drBlack["datakey"] = chkdt.Rows[r]["datakey"].ToString();
                            drBlack["reason"] = "被保険者番号なし";
                            dtBlack.Rows.Add(drBlack);
                        }

                        //ない場合エラー
                        if (chkdt.Rows[r]["hname"].ToString() == string.Empty)
                        {
                            DataRow drBlack = dtBlack.NewRow();
                            drBlack["datakey"] = chkdt.Rows[r]["datakey"].ToString();
                            drBlack["reason"] = "被保険者名なし";
                            dtBlack.Rows.Add(drBlack);
                        }

                        //ない場合エラー
                        if (chkdt.Rows[r]["pname"].ToString() == string.Empty)
                        {
                            DataRow drBlack = dtBlack.NewRow();
                            drBlack["datakey"] = chkdt.Rows[r]["datakey"].ToString();
                            drBlack["reason"] = "受療者名なし";
                            dtBlack.Rows.Add(drBlack);
                        }

                        //日付か数字がない場合エラー
                        if (!int.TryParse(chkdt.Rows[r]["pbirth"].ToString(), out int tmppb)&&
                            !DateTime.TryParse(chkdt.Rows[r]["pbirth"].ToString(), out DateTime tmpdtpd)
                            )
                        {
                            DataRow drBlack = dtBlack.NewRow();
                            drBlack["datakey"] = chkdt.Rows[r]["datakey"].ToString();
                            drBlack["reason"] = "受療者生年月日エラー" + chkdt.Rows[r]["pbirth"].ToString();
                            //sw.WriteLine("checkPersonalData:" + chkdt.Rows[r]["datakey"].ToString() + "受療者生年月日エラー");
                            dtBlack.Rows.Add(drBlack);
                        }

                        //日付か数字がない場合エラー
                        if (!int.TryParse(chkdt.Rows[r]["ym"].ToString(), out int tmpym) &&
                             !DateTime.TryParse(chkdt.Rows[r]["ym"].ToString(), out DateTime tmpdtym)
                             )
                        {
                            DataRow drBlack = dtBlack.NewRow();
                            drBlack["datakey"] = chkdt.Rows[r]["datakey"].ToString();
                            drBlack["reason"] = "診療年月エラー" + chkdt.Rows[r]["ym"].ToString();
                            // sw.WriteLine("checkPersonalData:" + chkdt.Rows[r]["datakey"].ToString() + "診療年月エラー");
                            dtBlack.Rows.Add(drBlack);
                        }


                        //ない場合エラー
                        if (chkdt.Rows[r]["country"].ToString() == string.Empty)
                        {
                            DataRow drBlack = dtBlack.NewRow();
                            drBlack["datakey"] = chkdt.Rows[r]["datakey"].ToString();
                            drBlack["reason"] = "国なし";

                            dtBlack.Rows.Add(drBlack);
                        }

                        //ない場合エラー
                        if (chkdt.Rows[r]["dname"].ToString() == string.Empty)
                        {
                            DataRow drBlack = dtBlack.NewRow();
                            drBlack["datakey"] = chkdt.Rows[r]["datakey"].ToString();
                            drBlack["reason"] = "傷病名なし";

                            dtBlack.Rows.Add(drBlack);
                        }

                        //数字でない、または「日」を削除して数字でない場合エラー
                        if (!int.TryParse(chkdt.Rows[r]["countdays"].ToString(), out int tmpcd) &&
                            !int.TryParse(chkdt.Rows[r]["countdays"].ToString().Replace("日",string.Empty), out int tmpcd1)
                            ) 
                        {
                            DataRow drBlack = dtBlack.NewRow();
                            drBlack["datakey"] = chkdt.Rows[r]["datakey"].ToString();
                            drBlack["reason"] = "診療実日数エラー" + chkdt.Rows[r]["countdays"].ToString();
                            //  sw.WriteLine("checkPersonalData:" + chkdt.Rows[r]["datakey"].ToString() + "診療実日数エラー");
                            dtBlack.Rows.Add(drBlack);
                        }

                        //ない場合エラー
                        if (chkdt.Rows[r]["datano"].ToString() == string.Empty)
                        {
                            DataRow drBlack = dtBlack.NewRow();
                            drBlack["datakey"] = chkdt.Rows[r]["datakey"].ToString();
                            drBlack["reason"] = "datano傷病名なし";

                            dtBlack.Rows.Add(drBlack);
                        }
                    }
                }
                catch(Exception ex)
                {
                  //  swForImport.WriteLine("CheckPersonalData:" + chkdt.Rows[r]["datano"].ToString() + "\n" + ex.Message);
                }

            }
            catch (NpgsqlException nex)
            {
               // swForImport.WriteLine("CheckPersonalData:" + chkdt.Rows[r]["datano"].ToString() + "\n" + nex.Message);                
            }
            finally
            {
                cmd.Dispose();//AWSコネクション制限対策
                dtBlack.AcceptChanges();
            }
        }

        /// <summary>
        /// 個人情報部登録
        /// </summary>
        /// <returns></returns>
        private bool entryPersonalData(Npgsql.NpgsqlTransaction tran,DataTable dtBlack)
        {

            Npgsql.NpgsqlCommand cmd = new NpgsqlCommand();            
            cmd.Connection = conn();
            cmd.Transaction = tran;
            cmd.CommandTimeout = 0;
            Npgsql.NpgsqlDataAdapter da = new NpgsqlDataAdapter();
            DataTable tmpdt = new DataTable();
            System.Text.StringBuilder sb = new StringBuilder();
            string strBlack = string.Empty;

            try
            {

                if (dtBlack.Rows.Count > 0)
                {

                    //一発挿入はエラーで落ちるのでforにする
                    foreach (DataRow drBlack in dtBlack.Rows)
                    {
                        if (drBlack["datakey"].ToString().Trim() != string.Empty)
                        {
                            strBlack += '\'' + drBlack["datakey"].ToString() + '\'';
                            strBlack += ',';
                        }
                    }
                    strBlack = strBlack.Substring(0, strBlack.Length - 1);
                }

                sb.Remove(0, sb.ToString().Length);
                sb.AppendLine("  select ");
                sb.AppendLine(" datakey , ");                                       //personalinfocd
                sb.AppendLine(" split_part(personaldata,',',12) sibu, ");           //支部
                sb.AppendLine(" split_part(personaldata,',',17) hihonum, ");        //被保番
                sb.AppendLine(" split_part(personaldata,',',22) hihoname, ");       //被保険者名
                sb.AppendLine(" split_part(personaldata,',',27) juryoname, ");      //受療者


                //受療者生年月日
                //excelのシリアル値を1900/1/1に足して日付とする

                //sb.AppendLine(" split_part(personaldata,',',32) juryobirthday, ");

                sb.AppendLine(" cast( ");
                sb.AppendLine(" case  ");
                sb.AppendLine(" when split_part(personaldata,',',32) ~'^[0-9]{5}' then ");
                sb.AppendLine(" cast(cast('1900/01/01' as date) + cast(split_part(personaldata,',',32) as int) as varchar) ");
                sb.AppendLine(" else ");
                sb.AppendLine(" split_part(personaldata,',',32) ");
                sb.AppendLine(" end ");
                sb.AppendLine("  as date) juryobirthday, ");


                //診療年月
                //excelのシリアル値を1900/1/1に足して日付とする。年月なので、更に日を削除
                sb.AppendLine(" case  ");
                sb.AppendLine(" when split_part(personaldata,',',19) ~'^[0-9]{5}' then ");
                sb.AppendLine(" substr(cast(cast('1900/01/01' as date) + cast(split_part(personaldata,',',19) as int) as varchar),1,7) ");
                sb.AppendLine(" else ");
                sb.AppendLine(" split_part(personaldata,',',19) ");                
                sb.AppendLine(" end ym, ");


                sb.AppendLine(" split_part(personaldata,',',34) country, ");    //国名
                sb.AppendLine(" split_part(personaldata,',',42) dname, ");      //傷病名

                //sb.AppendLine(" split_part(personaldata,',',50) countday, ");//診療実日数
                //20221125112143 furukawa st ////////////////////////
                //学校共済　診療日数が空欄の対処
                
                //sb.AppendLine(" cast(replace(split_part(personaldata,',',50),'日','') as int) countday, ");//診療実日数
                sb.AppendLine("case replace(split_part(personaldata,',',50),'日','')	when '' then 0 	else cast(replace(split_part(personaldata,',',50),'日','') as int )	end countday, ");//診療実日数
                //20221125112143 furukawa ed ////////////////////////


                sb.AppendLine(" datano, ");//ファイル連番
                sb.AppendLine(" ''  ");//予備

                sb.AppendLine(" from tblimport ");

                sb.AppendLine(" where importdate is null ");
                if(strBlack!=string.Empty) sb.AppendFormat(" and datakey not in ({0}) ",strBlack);

               

                sb.AppendLine(" group by  ");
                sb.AppendLine(" datakey , ");
                sb.AppendLine(" split_part(personaldata,',',12), ");
                sb.AppendLine(" split_part(personaldata,',',17), ");
                sb.AppendLine(" split_part(personaldata,',',22), ");
                sb.AppendLine(" split_part(personaldata,',',27), ");
                sb.AppendLine(" split_part(personaldata,',',32), ");
                sb.AppendLine(" split_part(personaldata,',',19), ");
                sb.AppendLine(" split_part(personaldata,',',34), ");
                sb.AppendLine(" split_part(personaldata,',',42), ");
                sb.AppendLine(" split_part(personaldata,',',50), ");
                sb.AppendLine(" datano");

                cmd.CommandText = sb.ToString();
                da.SelectCommand = cmd;
                da.Fill(tmpdt);

                int r = 0;
                foreach (DataRow dr in tmpdt.Rows)
                {
                    sb.Remove(0, sb.ToString().Length);
                    sb.AppendLine(" insert into tblPerson Values (");

                    sb.AppendFormat("'{0}',", dr["datakey"].ToString());
                    sb.AppendFormat("'{0}',", dr["sibu"].ToString());
                    sb.AppendFormat("'{0}',", dr["hihonum"].ToString());
                    sb.AppendFormat("'{0}',", dr["hihoname"].ToString());
                    sb.AppendFormat("'{0}',", dr["juryoname"].ToString());
                    sb.AppendFormat("'{0}',", dr["juryobirthday"].ToString());
                    sb.AppendFormat("'{0}',", dr["ym"].ToString());
                    sb.AppendFormat("'{0}',", dr["country"].ToString());
                    sb.AppendFormat("'{0}',", dr["dname"].ToString());
                    sb.AppendFormat("{0},", dr["countday"].ToString());
                    
                    sb.AppendFormat("'{0}',", dr["datano"].ToString());
                    sb.AppendFormat("'{0}')", string.Empty);

                    

                    cmd.CommandText = sb.ToString();

                    try
                    {
                        cmd.ExecuteNonQuery();
                        if (r % 300 == 0)
                        {
                            lbl.Text = $"個人情報部インポート{r}行";
                            Application.DoEvents();
                        }
                        r++;
                    }
                    catch (NpgsqlException nex)
                    {
                        swForImport.WriteLine("entryPersonalData:" + dr["datakey"].ToString() + "\n" + nex.Message);
                    }
                    catch (Exception ex)
                    {
                        swForImport.WriteLine("entryPersonalData:" + dr["datakey"].ToString() + ex.Message);
                    }
                }
                lbl.Text = $"個人情報部インポート{r}行";
                return true;

            }
            catch(NpgsqlException nex)
            {
                swForImport.WriteLine("entryPersonalData:" + sb.ToString()+"\n" + nex.Message);
                return false;
            }
            catch (Exception ex)
            {
                swForImport.WriteLine("entryPersonalData:" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }

            
        }

        /// <summary>
        /// 明細データ登録
        /// </summary>
        /// <returns></returns>
        private bool entryRowData(Npgsql.NpgsqlTransaction tran)
        {
            Npgsql.NpgsqlCommand cmd = new NpgsqlCommand();            
            cmd.Connection = conn();
            cmd.Transaction = tran;
            cmd.CommandTimeout = 0;
            Npgsql.NpgsqlDataAdapter da = new NpgsqlDataAdapter();
            DataTable tmpdt = new DataTable();
            
            try
            {
                System.Text.StringBuilder sb = new StringBuilder();
                sb.Remove(0, sb.ToString().Length);

                sb.AppendLine("  select  ");
                sb.AppendLine("  datano,  ");                                      //ファイル番号
                sb.AppendLine("  datakey,  ");                                     // 個人情報管理番号
                sb.AppendLine("  rowno,   ");                                      // 行番号
                sb.AppendLine("  split_part(rowdata,',',1) bunrui,  ");            // 明細書の「点数表分類」
                sb.AppendLine("  split_part(rowdata,',',2) komoku,  ");            // 明細書の「項目」
                sb.AppendLine("  split_part(rowdata,',',3) tensu,   ");            // 明細書の「点数」
                sb.AppendLine("  split_part(rowdata,',',4) suryo,   ");            // 明細書の「数量」
                sb.AppendLine("  split_part(rowdata,',',5) hokentensu,   ");       // 明細書の「保険点数」
                sb.AppendLine("  createdate,  ");                                  // yyyyMMdd HHmmss　ファイル情報から取得
                sb.AppendLine("  updatedate	 ");                                   // yyyyMMdd HHmmss　ファイル情報から取得
                sb.AppendLine("  from tblimport  ");
                sb.AppendLine("  where   ");
                sb.AppendLine("  (split_part(rowdata,',',3) not like '%合計%' and "); //項目、点数(単価)、数量、点数(合計）が入っているレコードを抽出
                sb.AppendLine("  split_part(rowdata,',',4)<>''  and  ");
                sb.AppendLine("  split_part(rowdata,',',5)<>'')  and  importdate is null ");
                sb.AppendLine("  or (trim(split_part(rowdata,',',2))<>''  and importdate is null)   ");
                

                // sb.AppendLine(" union all  ");
                sb.AppendLine(" union   ");

                sb.AppendLine("  select  ");
                sb.AppendLine("  datano,  ");                                      //ファイル番号
                sb.AppendLine("  datakey,  ");                                     // 個人情報管理番号
                sb.AppendLine("  rowno,   ");                                      // 行番号
                sb.AppendLine("  '' bunrui,  ");                                   // 明細書の「点数表分類」
                sb.AppendLine("  '' komoku,  ");                                   // 明細書の「項目」
                sb.AppendLine("  '合計(点)' tensu,   ");                           // 明細書の「点数」
                sb.AppendLine("  '' suryo,   ");                                   // 明細書の「数量」
                sb.AppendLine("  split_part(rowdata,',',5) hokentensu,   ");       // 明細書の「保険点数」
                sb.AppendLine("  createdate,  ");                                  // yyyyMMdd HHmmss　ファイル情報から取得
                sb.AppendLine("  updatedate	 ");                                   // yyyyMMdd HHmmss　ファイル情報から取得
                sb.AppendLine("  from tblimport  ");
                sb.AppendLine("  where   ");
                sb.AppendLine("  split_part(rowdata,',',3) like '%合計%' ");       //合計が入っているレコードを抽出
                sb.AppendLine("  and importdate is null");

                sb.AppendLine(" union  ");
                //sb.AppendLine(" union all  ");


                sb.AppendLine("  select  ");
                sb.AppendLine("  datano,  ");                                    //ファイル番号
                sb.AppendLine("  datakey,  ");                                   // 個人情報管理番号     
                sb.AppendLine("  rowno,   ");                                    // 行番号     
                sb.AppendLine("  '' bunrui,  ");                                 // 本来は明細書の「点数表分類」だが、備考だけレコードなので空欄にする
                sb.AppendLine("  split_part(rowdata,',',1) komoku,  ");          // 明細書の「項目」     に備考の文字列を入れる
                sb.AppendLine("  '' tensu,   ");                                 // 本来は明細書の「点数」     だが、備考だけレコードなので空欄にする
                sb.AppendLine("  '' suryo,   ");                                 // 本来は明細書の「数量」     だが、備考だけレコードなので空欄にする
                sb.AppendLine("  '' hokentensu,   ");                            // 本来は明細書の「保険点数」   だが、備考だけレコードなので空欄にする  
                sb.AppendLine("  createdate,  ");                                // yyyyMMdd HHmmss　ファイル情報から取得     
                sb.AppendLine("  updatedate	 ");                                 // yyyyMMdd HHmmss　ファイル情報から取得     
                sb.AppendLine("  from tblimport  ");                             
                sb.AppendLine("  where   ");
                sb.AppendLine("  rowdata like '%備考%'   ");                     //「備考」が入っているレコードを抽出
                sb.AppendLine("  and importdate is null");
              //  sb.AppendLine("  group by datano,datakey,rowno,rowdata,createdate,updatedate	 ");

                sb.AppendLine("  order by datano,rowno");


                //sb.AppendLine(" split_part(rowdata,',',3)  <>'合計（点）' and  ");
                //sb.AppendLine(" split_part(rowdata,',',2)  <>'' and  ");

                //sb.AppendLine(" split_part(rowdata, ',', 3)~'[0-9,]' and ");
                //sb.AppendLine(" split_part(rowdata, ',', 4)~'[0-9,]' and ");
                //sb.AppendLine(" split_part(rowdata, ',', 5)~'[0-9,]'");

                cmd.CommandText = sb.ToString();
                da.SelectCommand = cmd;
                da.Fill(tmpdt);

                int r = 0;
                foreach(DataRow dr in tmpdt.Rows)
                {
                    sb.Remove(0, sb.ToString().Length);
                    sb.AppendLine(" insert into tblData Values (");

                    sb.AppendFormat("'{0}',", dr["datano"].ToString());             //ファイル番号
                    sb.AppendFormat("'{0}',", dr["datakey"].ToString());            // 個人情報管理番号
                    sb.AppendFormat("{0},", dr["rowno"].ToString());                // 行番号
                    sb.AppendFormat("'{0}',", dr["bunrui"].ToString());             // 明細書の「点数表分類」
                    sb.AppendFormat("'{0}',", dr["komoku"].ToString());             // 明細書の「項目」
                    sb.AppendFormat("'{0}',", dr["tensu"].ToString());               // 明細書の「点数」
                    sb.AppendFormat("'{0}',", dr["suryo"].ToString());               // 明細書の「数量」
                    sb.AppendFormat("'{0}',", dr["hokentensu"].ToString());          // 明細書の「保険点数」
                    sb.AppendFormat("'{0}',", dr["createdate"].ToString());          // yyyyMMdd HHmmss　ファイル情報から取得
                    sb.AppendFormat("'{0}')", dr["updatedate"].ToString());          // yyyyMMdd HHmmss　ファイル情報から取得
  
                    cmd.CommandText = sb.ToString();
                    
                    try
                    {
                        
                        cmd.ExecuteNonQuery();

                        if (r % 300 == 0)
                        {
                            lbl.Text = $"明細部インポート{r}行";
                            Application.DoEvents();
                        }
                        r++;
                    }
                    catch (NpgsqlException nex)
                    {
                        swForImport.WriteLine("entrRowData:" + dr["datakey"].ToString() + sb.ToString() + "\n" + nex.Message);
                    }
                    catch (Exception ex)
                    {
                        swForImport.WriteLine("entrRowData:" + dr["datakey"].ToString() + sb.ToString() + ex.Message);
                    }
                }
                lbl.Text = $"明細部インポート{r}行";


                //sb.AppendLine(" insert into tbldata ");
                //sb.AppendLine(" select ");
                //sb.AppendLine(" datano, ");                         //ファイル番号
                //sb.AppendLine(" datakey, ");                        // 個人情報管理番号
                //sb.AppendLine(" rowno,  ");                         // 行番号
                //sb.AppendLine(" split_part(rowdata,',',1), ");      // 明細書の「点数表分類」
                //sb.AppendLine(" split_part(rowdata,',',2), ");      // 明細書の「項目」
                //sb.AppendLine(" cast( (case when split_part(rowdata,',',3) <>'' then  split_part(rowdata,',',3) else null end) as int),  ");     // 明細書の「点数」
                //sb.AppendLine(" cast( (case when split_part(rowdata,',',4) <>'' then  split_part(rowdata,',',4) else null end) as int),  ");     // 明細書の「数量」
                //sb.AppendLine(" cast( (case when split_part(rowdata,',',5) <>'' then  split_part(rowdata,',',3) else null end) as int),  ");     // 明細書の「保険点数」
                //sb.AppendLine(" createdate, ");     // yyyyMMdd HHmmss　ファイル情報から取得
                //sb.AppendLine(" updatedate	");     // yyyyMMdd HHmmss　ファイル情報から取得
                //sb.AppendLine(" from tblimport ");
                //sb.AppendLine(" where  ");
                //sb.AppendLine(" split_part(rowdata,',',3)  <>'合計（点）' and  ");
                //sb.AppendLine(" split_part(rowdata,',',2)  <>'' and  ");

                //sb.AppendLine(" split_part(rowdata, ',', 3)~'[0-9,]' and ");
                //sb.AppendLine(" split_part(rowdata, ',', 4)~'[0-9,]' and ");
                //sb.AppendLine(" split_part(rowdata, ',', 5)~'[0-9,]'");

                //cmd.CommandText = sb.ToString();
                //cmd.ExecuteNonQuery();


                return true;
            }

            catch (NpgsqlException nex)
            {
                swForImport.WriteLine("entryRowData:" + "\n" + nex.Message);                
                MessageBox.Show(nex.Message);
                return false;
            }
            catch (Exception ex)
            {
                swForImport.WriteLine("entryRowData:" + ex.Message);                
                MessageBox.Show(ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
            }
            
            
        }
        
        /// <summary>
        /// ファイル情報登録
        /// </summary>
        /// <returns></returns>
        private bool entryFileInfo(Npgsql.NpgsqlTransaction tran)
        {
            Npgsql.NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.Connection = conn();
            cmd.Transaction = tran;
            cmd.CommandTimeout = 0;
            Npgsql.NpgsqlDataAdapter da = new NpgsqlDataAdapter();
            DataTable tmpdt = new DataTable();

            try
            {
          

                System.Text.StringBuilder sb = new StringBuilder();

               // sb.AppendLine(" insert into tblfile  ");
                sb.AppendLine(" select ");
                sb.AppendLine(" fullpath, ");
                sb.AppendLine(" '' serverpath, ");
                sb.AppendLine(" modifypath, ");
                sb.AppendLine(" '' filename, ");
                sb.AppendLine(" datano, ");
                sb.AppendLine(" datakey ");

                sb.AppendLine(" from ");
                sb.AppendLine(" tblimport ");

                sb.AppendLine(" where importdate is null");

                sb.AppendLine(" group by  ");
                sb.AppendLine(" fullpath, ");
                sb.AppendLine(" modifypath, ");
                sb.AppendLine(" datano, ");
                sb.AppendLine(" datakey ");

                cmd.CommandText = sb.ToString();
                da.SelectCommand = cmd;
                da.Fill(tmpdt);

                int r = 0;
                foreach (DataRow dr in tmpdt.Rows)
                {
                    sb.Remove(0, sb.ToString().Length);
                    sb.AppendLine(" insert into tblFile Values (");

                    sb.AppendFormat("'{0}',", dr["fullpath"].ToString());
                    sb.AppendFormat("'{0}',", dr["serverpath"].ToString());
                    sb.AppendFormat("'{0}',", dr["modifypath"].ToString());
                    
                    sb.AppendFormat("'{0}',", dr["filename"].ToString());
                    sb.AppendFormat("'{0}',", dr["datano"].ToString());
                    sb.AppendFormat("'{0}')", dr["datakey"].ToString());

                    cmd.CommandText = sb.ToString();

                    try
                    {

                        cmd.ExecuteNonQuery();

                        if (r % 100 == 0)
                        {
                            lbl.Text = $"ファイル情報{r}行";
                            Application.DoEvents();
                        }
                        r++;
                    }
                    catch (NpgsqlException nex)
                    {
                        swForImport.WriteLine("entryFileInfo:" + dr["datakey"].ToString() + sb.ToString() + "\n" + nex.Message);
                    }
                    catch (Exception ex)
                    {
                        swForImport.WriteLine("entryFileInfo:" + dr["datakey"].ToString() + sb.ToString() + ex.Message);
                    }

                }
                lbl.Text = $"ファイル情報{r}行";

                return true;
            }
            catch (NpgsqlException nex)
            {
                swForImport.WriteLine("entryFileInfo:" + "\n" + nex.Message);
                return false;
            }
            catch (Exception ex)
            {
                swForImport.WriteLine("entryFileInfo:" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();//AWSコネクション制限対策
            }

            
        }


     
        string strPathLog = System.IO.Path.GetDirectoryName(Application.ExecutablePath) + "\\log";
        string strPathBlackList = System.IO.Path.GetDirectoryName(Application.ExecutablePath) + "\\BlackList";
        
        
    
        /// <summary>
        /// ブラックリスト(不具合あるファイルのリスト)         
        /// </summary>
        /// <param name="dtBlack"></param>
        private void CreateBlackListFile(DataTable dtBlack)
        {
            System.IO.StreamWriter sw = new System.IO.StreamWriter($"{strPathBlackList}\\{DateTime.Now.ToString("yyyyMMdd-HHmmss")}_BlackList.txt");
            foreach(DataRow dr in dtBlack.Rows)
            {
                sw.WriteLine(dr["datakey"].ToString() + ',' + dr["reason"].ToString());
            }
            sw.Close();

        }

        /// <summary>
        /// インポート完了した日を入れる
        /// </summary>
        /// <param name="tran"></param>
        /// <returns></returns>
        private bool UpdateImportDate(NpgsqlTransaction tran)
        {
            Npgsql.NpgsqlCommand cmd = new NpgsqlCommand();
            
            cmd.Connection = conn();
            cmd.Transaction = tran;
            cmd.CommandTimeout = 0;
            try
            {
                cmd.CommandText = $"update tblimport set importdate='{DateTime.Now.ToString("yyyy-MM-dd")}' where importdate is null";
                cmd.ExecuteNonQuery();
                

                return true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();//AWSコネクション制限対策
                conn().Close();
            }
        }

        /// <summary>
        /// 診療年月をyyyy-mmに更新
        /// </summary>
        /// <param name="tran"></param>
        /// <returns></returns>
        private bool UpdateYM(NpgsqlTransaction tran)
        {
            Npgsql.NpgsqlCommand cmd = new NpgsqlCommand();
            
            cmd.Connection = conn();
            cmd.Transaction = tran;
            cmd.CommandTimeout = 0;
            try
            {
                cmd.CommandText = $"update tblperson set ym=replace(ym,'/','-') where trim(ym) <>''";
                cmd.ExecuteNonQuery();

                cmd.CommandText = $"update tblperson set ym=substr(ym,0,8) where trim(ym) <>'' and length(ym)>7 ";
                cmd.ExecuteNonQuery();

                return true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();//AWSコネクション制限対策
                conn().Close();
            }
        }

        /// <summary>
        /// 県番号リスト
        /// </summary>
        /// <returns></returns>
        private bool CreatePrefList()
        {
            Npgsql.NpgsqlCommand cmd = new NpgsqlCommand();
            Npgsql.NpgsqlTransaction tran = conn().BeginTransaction();
            Npgsql.NpgsqlDataAdapter da = new NpgsqlDataAdapter();
            
            cmd.Connection = conn();

            try
            {
                cmd.CommandText = "select * from tblpref";
                da.SelectCommand = cmd;
                da.Fill(dtPref);

                return true;

            }
            catch(Exception ex)
            {
                return false;
            }
            finally
            {
                conn().Close();
            }
            
        }

        /// <summary>
        /// エクセルがあるフォルダ探索（再帰呼び出し
        /// </summary>
        /// <param name="strpath"></param>
        /// <param name="dt"></param>
        private void getexcel(string strpath,DataTable dt)
        {
            String[] strdir=System.IO.Directory.GetDirectories(strpath);
            foreach(string s in strdir)
            {
                getexcel(s,dt);
            }
            

            String[] strexcel=System.IO.Directory.GetFiles(strpath, "*.xlsx");
            
            foreach (string e in strexcel)
            {                
                //一時ファイルをスキップ
                if (e.Contains("$")) continue;
                //無関係ファイルをスキップ
                if (e.Contains("確認事項")) continue;

                if (e.Contains("レセプト作成者受け渡し表")) continue; 


                loadexcel(e, exceldatano,dt);
                exceldatano++;

                if (exceldatano % 300 == 0)
                {
                    lbl.Text = System.IO.Path.GetFileName(strpath);
                    Application.DoEvents();
                }

            }

        }

        /// <summary>
        /// データがあるシートを検索
        /// </summary>
        /// <param name="w">ワークブック</param>
        /// <returns>シートインデックス番号</returns>
        private int CheckWorksheet(NPOI.SS.UserModel.IWorkbook w)
        {

            int sheetcnt = 0;
            for(sheetcnt=0;sheetcnt<10;sheetcnt++)
            {
                NPOI.SS.UserModel.ISheet s = w.GetSheetAt(sheetcnt);

                //マージセル解除
                for (int cnt = 0; cnt < s.NumMergedRegions; cnt++) s.RemoveMergedRegion(cnt);

                //個人情報部分 セル範囲決まってる
                NPOI.SS.Util.CellRangeAddress rng = new NPOI.SS.Util.CellRangeAddress(1, 13, 1, 6);

                for (int r = rng.FirstRow; r < rng.LastRow; r++)
                {
                    if (s.GetRow(r) == null) continue;

                    for (int c = rng.FirstColumn; c < rng.LastColumn; c++)
                    {
                        NPOI.SS.UserModel.ICell cc = s.GetRow(r).GetCell(c);
                        if (retVal(cc).ToString().Trim() == "療養者氏名")
                        {
                            return sheetcnt;
                        }
                      
                    }
                }

            }
            return -1;

        }

        /// <summary>
        /// エクセル開き、必要なデータを取得しCSV行を作成し、リストに貯める
        /// </summary>
        /// <param name="strpath">ファイルパス</param>
        /// <param name="exceldatano">連番</param>
        private void loadexcel(string strpath,int exceldatano,DataTable dt)
        {

            NPOI.SS.UserModel.IWorkbook w;//excelワークブック

            try
            {
                //壊れてて開けない場合は飛ばす
                w = NPOI.SS.UserModel.WorkbookFactory.Create(strpath);
                if (w.NumberOfSheets < 1) return;
            }
            catch(Exception ex)
            {
                return;
            }
            

            int rowno = 1;                                      //１エクセルファイル内行番号
            List<String> lstData = new List<string>();          //ファイル出力用リスト
            string tmp = string.Empty;                          //一時取得
            string strperson = string.Empty;                    //個人情報部分文字列
            string strFilePathOrig = strpath;                   //ファイルパスオリジナル
            string strKey = string.Empty;                       //キー値

            System.IO.FileInfo fi = new System.IO.FileInfo(strFilePathOrig);
            DateTime dtCreate=fi.CreationTime;                   //作成日
            DateTime dtLastWriteTime = fi.LastWriteTime;         //最終更新日

            string strFilePathModify = CreateFilePath(strpath);//ファイルパス余計な文字を省いた

            NPOI.SS.UserModel.ISheet s;
            try
            {
                //s = w.GetSheetAt(w.GetSheetIndex("計算書"));//excelシート 計算書だけを取得
                //s = w.GetSheetAt(0);//excelシート　計算書だけではないので、無条件で１シート目選択
                int idxsheet = CheckWorksheet(w);
                s = w.GetSheetAt(idxsheet);//excelシート　計算書だけではないので、無条件で１シート目選択
            }
            catch(Exception ex)
            {
                swForImport.WriteLine(strpath + ex.Message);                
                return;
            }
            
            //NPOI.SS.UserModel.ISheet s = w.GetSheetAt(0);//excelシート
          
            NPOI.SS.UserModel.IRow ir = null;
                        
            //マージセル解除
            for(int cnt=0;cnt< s.NumMergedRegions; cnt++) s.RemoveMergedRegion(cnt);
                            
            //個人情報部分 セル範囲決まってる
            NPOI.SS.Util.CellRangeAddress rng = new NPOI.SS.Util.CellRangeAddress(1, 13, 1, 6);
            


            for (int r = rng.FirstRow; r < rng.LastRow; r++)
            {
                if (s.GetRow(r) == null) continue;

                for (int c = rng.FirstColumn; c < rng.LastColumn; c++)
                {
                    
                    NPOI.SS.UserModel.ICell cc = s.GetRow(r).GetCell(c);
                    if (retVal(cc).ToString().Trim() != String.Empty)
                    {
                        tmp += retVal(cc) + ',';
                    }
                    else
                    {
                        tmp += string.Empty + ',';
                    }
                }                
            }


            strperson = tmp;
            //strperson =  ",dataseparator," + tmp;
            //lstPerson.Add(exceldatano + ",p," + tmp);


            tmp = string.Empty;
            string strRow = string.Empty;//明細の1行の文字列


            strKey = CreateKey(strFilePathOrig, strperson, tmp);
            //20220414131318 furukawa st ////////////////////////
            //学校共済　キー作成エラー時、ログにファイル名を出しスキップ
            
            if (strKey == string.Empty)
            {
                swForImport.WriteLine(strFilePathOrig + "CreateKeyエラー");
                w.Close();
                return;
            }
            //20220414131318 furukawa ed ////////////////////////

            //レセ部分
            NPOI.SS.Util.CellRangeAddress rngdata = new NPOI.SS.Util.CellRangeAddress(14,200, 1, 6);
            for (int r = rngdata.FirstRow; r < rngdata.LastRow; r++)
            {
                if (s.GetRow(r) == null) continue;
                                
                //行ループ
                for (int c = rngdata.FirstColumn; c < rngdata.LastColumn; c++)
                {                    
                    NPOI.SS.UserModel.ICell cc = s.GetRow(r).GetCell(c);
                    if (retVal(cc).ToString().Trim() != String.Empty)
                    {
                        tmp += retVal(cc) + ',';
                    }
                    else
                    {
                        tmp += string.Empty + ',';
                    }
                }

            //空欄でない場合のみ取得
            //  if (tmp.Replace(",", "").Trim() != string.Empty)
            //  {
                strRow = tmp;

                //string strLst = strFilePathOrig + "," + strFilePathModify + "," + exceldatano + "," +
                //            strperson + ",dataseparator," + rowno + "," + strRow;

                try
                {
                    DataRow dr = dt.NewRow();

                    dr["fullpath"] = strFilePathOrig;
                    dr["modifypath"] = strFilePathModify;

                    dr["datano"] = exceldatano;
                    dr["datakey"] = strKey;

                    dr["personaldata"] = strperson;
                    dr["rowno"] = rowno;
                    dr["rowdata"] = strRow;

                    dr["createdate"] = dtCreate;
                    dr["updatedate"] = dtLastWriteTime;


                    dt.Rows.Add(dr);

                    //lstData.Add(strKey + ',' + strLst);

                    rowno++;
                    // }
                    tmp = "";
                }
                catch(Exception ex)
                {
                    swForImport.WriteLine(strFilePathOrig +  ex.Message);
                }
            }

            //lstData.Add(tmp);// strperson + ",datafields," + exceldatano + ',' + rowno + "," + strrow);
            w.Close();

           
           

            //createtxt( lstData);
        }

     

        //ファイルパスの余計な文字を削除
        private string CreateFilePath(String strPath)
        {
            string res = string.Empty;
            string strServer = string.Empty;
            string strPathModify = string.Empty;
            string strExt = string.Empty;


            //20220413171852 furukawa st ////////////////////////
            //サーバIP変更対応
            
            strServer = "\\\\192.168.110.14\\kaigai_archive\\学校共済\\納品済";
            //  strServer = "\\\\192.168.112.78\\tenken\\03海外療養費\\日本システム\\公立学校共済\\";
            //  strServer = "\\\\192.168.112.78\\kaigai_archive\\学校共済\\納品済";
            //20220413171852 furukawa ed ////////////////////////

            strPathModify = strPath.Substring(strServer.Length);
            strPathModify = strPathModify.Replace("日本ｼｽﾃﾑ様(学校共済)", string.Empty);
            //strExt = System.IO.Path.GetExtension(strPathModify);
            //strPathModify = System.IO.Path.GetFileNameWithoutExtension(strPathModify);


            strPathModify = System.Text.RegularExpressions.Regex.Replace(strPathModify, "支部[(（)）]", string.Empty);
            strPathModify = System.Text.RegularExpressions.Regex.Replace(strPathModify, "支部.+件", string.Empty);
            strPathModify = System.Text.RegularExpressions.Regex.Replace(strPathModify, "支部.[0-9]{1,2}件", string.Empty);
            strPathModify = System.Text.RegularExpressions.Regex.Replace(strPathModify, "支部", string.Empty);
            
            //strPathModify = System.Text.RegularExpressions.Regex.Replace(strPathModify, "[(（]", string.Empty);


            strPathModify = System.Text.RegularExpressions.Regex.Replace(strPathModify, "①", "_01");//丸数字削除
            strPathModify = System.Text.RegularExpressions.Regex.Replace(strPathModify, "②", "_02");//丸数字削除
            strPathModify = System.Text.RegularExpressions.Regex.Replace(strPathModify, "③", "_03");//丸数字削除
            strPathModify = System.Text.RegularExpressions.Regex.Replace(strPathModify, "④", "_04");//丸数字削除
            strPathModify = System.Text.RegularExpressions.Regex.Replace(strPathModify, "⑤", "_05");//丸数字削除
            strPathModify = System.Text.RegularExpressions.Regex.Replace(strPathModify, "⑥", "_06");//丸数字削除
            strPathModify = System.Text.RegularExpressions.Regex.Replace(strPathModify, "⑦", "_07");//丸数字削除
            strPathModify = System.Text.RegularExpressions.Regex.Replace(strPathModify, "⑧", "_08");//丸数字削除
            strPathModify = System.Text.RegularExpressions.Regex.Replace(strPathModify, "⑨", "_09");//丸数字削除
            strPathModify = System.Text.RegularExpressions.Regex.Replace(strPathModify, "⑩", "_10");//丸数字削除


            strPathModify = System.Text.RegularExpressions.Regex.Replace(strPathModify, "（[0-9]{1,2}件）", string.Empty);
            strPathModify = System.Text.RegularExpressions.Regex.Replace(strPathModify, "[（(]", "_");//開きカッコはアンダーバー
            strPathModify = System.Text.RegularExpressions.Regex.Replace(strPathModify, "[）)　]", string.Empty);//閉じカッコ、全角スペース削除
          

            res = strPathModify;
            
            return res;
        }

        /// <summary>
        /// レセ単位のキー文字列作成
        /// </summary>
        /// <param name="strPath"></param>
        /// <param name="strPerson"></param>
        /// <param name="strDetail"></param>
        /// <returns></returns>
        private string CreateKey(string strPath,string strPerson,string strDetail)
        {
            string strNouhin=string.Empty;                  //納品日
            string strSinryoYM = string.Empty;              //診療年月
            string strHihonum = string.Empty;               //被保険者番号
            string strShibu = string.Empty;                 //支部名
            string strKey = string.Empty;                   //レセ単位キー
            string strSinryoka = string.Empty;              //診療科
            string strNyuin = string.Empty;                 //入院外来

            try
            {
                if (System.Text.RegularExpressions.Regex.IsMatch(strPath, "[0-9]{8}再?納品"))
                {
                    //パスから納品日付取得
                    System.Text.RegularExpressions.MatchCollection m =
                        System.Text.RegularExpressions.Regex.Matches(strPath, "[0-9]{8}再?納品");
                    strNouhin = m[0].Value;
                }
                else if (System.Text.RegularExpressions.Regex.IsMatch(strPath, "[0-9]{8}_.+確認結果"))
                {
                    //JAST確認結果も取得
                    System.Text.RegularExpressions.MatchCollection m =
                        System.Text.RegularExpressions.Regex.Matches(strPath, "[0-9]{8}_.+確認結果");
                    strNouhin = m[0].Value;
                }
                else
                {
                    strNouhin = "99999999";
                }



                strShibu = strPerson.Split(',')[11].ToString();//支部名
                strHihonum = strPerson.Split(',')[16].ToString();//被保番

                strSinryoYM = strPerson.Split(',')[18].ToString();//診療年月


                //日付そのまま取れる場合もある
                //if (!int.TryParse(strSinryoYM, out int t)) return string.Empty;
                if (!DateTime.TryParse(strSinryoYM, out DateTime tmpdt) && int.TryParse(strSinryoYM, out int tmpint))
                    strSinryoYM = new DateTime(1900, 1, 1).AddDays(int.Parse(strSinryoYM)).ToString("yyyyMM");//excelのシリアル値をdatetime型に直す
                if (DateTime.TryParse(strSinryoYM, out DateTime tmpdt1))
                    strSinryoYM = tmpdt1.ToString("yyyyMM");


                //診療科取得
                if (strPerson.Contains("医科")) strSinryoka = "医科";
                if (strPerson.Contains("歯科")) strSinryoka = "歯科";


                //内容からは判定できない
                //入院
                //if (strDetail.Contains("(90) 入院")) strNyuin = "入院";


                //支部名を県番号に置き換え
                int intPrefNo = 0;
                DataRow[] dr = dtPref.Select("pref='" + strShibu.Replace("支部", string.Empty) + "'");
                if (!int.TryParse(dr[0]["prefno"].ToString(), out intPrefNo)) intPrefNo = 99;


                //キー作成 納品日付_支部名_被保険者番号_診療年月_診療科入外            
                //20200101_14_公立神奈川1234567_201912_医科入院

                strKey = $"{strNouhin}_{intPrefNo.ToString("00")}_{strHihonum}_{strSinryoYM}_{strSinryoka}{strNyuin}";
                //strKey = $"{strNouhin}_{strShibu}_{strHihonum}_{strSinryoYM}_{strSinryoka}{strNyuin}";

                //余計な文字を削除
                strKey = System.Text.RegularExpressions.Regex.Replace(strKey, "[\\s・]", string.Empty);
                //半角英数
                strKey = Microsoft.VisualBasic.Strings.StrConv(strKey, Microsoft.VisualBasic.VbStrConv.Narrow);


                return strKey;
            }
            catch(Exception ex)
            {
                //上の関数でログに出す
                //MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + strPath + "\r\n" + ex.Message);
                return string.Empty;
            }
        }

     

        //テキストファイル作成
        private void createtxt(List<String> lstData)
        {          
            int count = lstData.Count;
            for (int r = 0; r < count; r++)
            {
                swForImport.WriteLine(lstData[r].ToString());
            }                        
        }


        //セル値の取得
        private string retVal(NPOI.SS.UserModel.ICell cell)
        {
            if (cell == null) return string.Empty;
            
            switch (cell.CellType.ToString())
            {
                case "Text":
                case "String":
                    return cell.StringCellValue.Trim();
                case "Numeric":
                    
                    //20200109122708 furukawa st ////////////////////////
                    //日付を判定
                    
                    if (NPOI.SS.UserModel.DateUtil.IsCellDateFormatted(cell))
                    {
                        return cell.DateCellValue.ToString("yyyy/MM/dd");
                    }
                    else
                    {
                        return cell.NumericCellValue.ToString().Trim();
                    }
                    
                    //return cell.NumericCellValue.ToString().Trim();
                //20200109122708 furukawa ed ////////////////////////
                case "Formula":
                    if (cell.NumericCellValue.ToString() == "0") return string.Empty;
                    return cell.NumericCellValue.ToString();

                default:
                    return string.Empty;
            }
        }
        #endregion

      

        /// <summary>
        /// 学校共済　該当被保険者番号の診療年月をセット
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txthnum_Validating(object sender, CancelEventArgs e)
        {
            setYM(txthnum.Text);
        }

        private void txthnum_TextChanged(object sender, EventArgs e)
        {
            cmbYM.Text = string.Empty;

        }


        List<string> lstFiles = new List<string>();

        private void buttonImp_Click(object sender, EventArgs e)
        {
            //2021年5月5日
            //アーカイブにある、2017年フォルダを指定し、全エクセルレセ作成ファイルを取得することに成功
            //debug\detail.txt とhead.txtに　|　区切りで取得した。
            //あとはそのテキストをテーブルにいれて、そこから印刷物を出す試験をしないと
            //入力画面を作っても意味がない


            //フォルダからエクセルパス取得                       
            getFilePaths();

          
        }

        private void getFilePaths()
        {
            ReadDoc.Utils.DialogUtility.OpenFolderDialog d = new Utils.DialogUtility.OpenFolderDialog();
            if (d.ShowDialog() == DialogResult.Cancel) return ;

            if (d.FolderPath == string.Empty) return ;

            getFilesForKK(d.FolderPath);

            MessageBox.Show("end");
        }
       
        private void getFilesForKK(string strPath)
        {
            string[] arrdir=System.IO.Directory.GetDirectories(strPath);
            foreach(string s in arrdir)
            {                
                getFilesForKK(s);
            }


            lstFiles=System.IO.Directory.GetFiles(strPath).ToList<string>();
            foreach (string s in lstFiles)
            {
                readFiles(s);
            }

            
        }

        System.IO.StreamWriter swh;// = new System.IO.StreamWriter("head.txt",false);
        System.IO.StreamWriter swd;// = new System.IO.StreamWriter("detal.txt",false);
        int no = 0;
        private bool readFiles(string strFileName)
        {
            if ((System.IO.Path.GetExtension(strFileName) != ".xlsx" &&
                System.IO.Path.GetExtension(strFileName) != ".xls" )||
                strFileName.Contains("$") ) return false;
            
            if (!System.Text.RegularExpressions.Regex.IsMatch(System.IO.Path.GetFileName(strFileName), "[0-9-]{10}.+")) return false;
            
            System.IO.FileStream fs = new System.IO.FileStream(strFileName, System.IO.FileMode.Open);
            NPOI.SS.UserModel.IWorkbook wb=null;// = NPOI.SS.UserModel.WorkbookFactory.Create(fs);
            NPOI.SS.UserModel.ISheet ws;// = wb.GetSheet("別紙2");
            //if (ws == null) return true;

            int rownumber = 0;

            try
            {
                wb = NPOI.SS.UserModel.WorkbookFactory.Create(fs);
                ws = wb.GetSheet("別紙2");
                if (ws == null) return true;

                string info = string.Empty;
                string dtl = string.Empty;

                swh = new System.IO.StreamWriter("head.txt", true);
                swd = new System.IO.StreamWriter("detail.txt", true);
                

                //info
                for (int r = 0; r < 14; r++)
                {
                    NPOI.SS.UserModel.IRow irow = ws.GetRow(r);

                    if (irow == null) continue;

                    info += strFileName + '|';
                    info += no.ToString() + '|';

                    for (int c = 1; c < irow.LastCellNum; c++)
                    {

                        NPOI.SS.UserModel.ICell icell = irow.GetCell(c);

                        

                        if (icell == null)
                        {
                            continue;
                            //info += '|';
                        }
                        else if (icell.CellType == NPOI.SS.UserModel.CellType.Numeric)
                        {
                            if (icell.NumericCellValue.ToString().Trim() == string.Empty) continue;
                            info +=icell.NumericCellValue.ToString()+'|';
                        }
                        else if (icell.CellType == NPOI.SS.UserModel.CellType.String)
                        {
                            //info+=icell.StringCellValue.ToString() + '|';
                            if (icell.StringCellValue.Trim() == string.Empty) continue;
                            info += System.Text.RegularExpressions.Regex.Replace(icell.StringCellValue, "[\r\n|\r|\n]", "") + '|';
                        }
                        else
                        {
                            continue;
                            //info += '|';
                        }

                    }
                }

                string biko = string.Empty;
                string tensubunrui = string.Empty;

                //detail
                for (int r = 14; r < ws.LastRowNum; r++)
                {
                    NPOI.SS.UserModel.IRow irow = ws.GetRow(r);

                    if (irow == null) continue;
                    dtl += strFileName + '|';
                    dtl += no.ToString()+'|';
                    dtl += (rownumber++.ToString()) + '|';

                    for (int c = 1; c <6; c++)
                    {

                        NPOI.SS.UserModel.ICell icell = irow.GetCell(c);

                        if (icell == null)
                        {
                            dtl += '|';
                            
                        }
                        else if (icell.CellType == NPOI.SS.UserModel.CellType.Numeric)
                        {
                            dtl+=icell.NumericCellValue.ToString() + '|';
                        }
                        else if (icell.CellType == NPOI.SS.UserModel.CellType.String)
                        {

                            if (icell.StringCellValue.Contains("【備考】"))
                            {
                                //biko += icell.StringCellValue;
                                biko+=System.Text.RegularExpressions.Regex.Replace(icell.StringCellValue, "[\r\n|\r|\n]", "");
                                
                            }
                            else if(biko!=string.Empty && icell.StringCellValue.Contains("点数表分類"))
                            {
                                //何も入れない
                            }
                            else if (System.Text.RegularExpressions.Regex.IsMatch(icell.StringCellValue, "^\\([0-9]{2}\\).+"))
                            {
                                //(21)とかの点数分類番号を記憶して次レコードに引き継ぐ
                                tensubunrui = icell.StringCellValue;
                                dtl += icell.StringCellValue.ToString().Trim() + '|';
                            }
                           
                            else
                            {
                                dtl += icell.StringCellValue.ToString().Trim() + '|';
                            }
                        }
                        else if (icell.CellType==NPOI.SS.UserModel.CellType.Blank && c == 1 && icell.StringCellValue == string.Empty)
                        {
                            //1列目で空欄の場合、上で控えた(21)とかの点数分類番号を表示

                            //2列目の詳細が空欄の場合は点数分類番号を入れない
                            NPOI.SS.UserModel.ICell icell2 = irow.GetCell(2);
                            if (icell2!=null && icell2.CellType != NPOI.SS.UserModel.CellType.Blank)
                            {
                                dtl += tensubunrui + '|';// + icell.StringCellValue.ToString() + '|';
                            }
                            else
                            {
                                dtl += '|';
                            }
                            
                        }                     
                        else
                        {
                            dtl += '|';
                        }

                    }
                    swd.WriteLine(dtl);
                    dtl = "";
                    
                }
                no++;

                swh.WriteLine(info);
                
                
                return true;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            finally
            {
                wb.Close();
                swh.Close();
                swd.Close();
                fs.Close();
                
            }

        }

     
    }
}

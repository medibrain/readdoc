﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadDoc
{

    public partial class clsKKData
    {


        public class ManageInfo
        {


            public string strKoban { get; set; }
            public string strBranchNumber { get; set; }
            public string strRezeptCount { get; set; }
            public string strCreator { get; set; }
            public string strPageForRezept { get; set; }
            public string strShibu { get; set; }
            public string strHihoName { get; set; }
            public string strHihoMark { get; set; }
            public string strHihoNum { get; set; }
            public string strShinryoYM { get; set; }
            public string strPatientName { get; set; }
            public string strPatientBirthday { get; set; }
            public string strStartDate { get; set; }
            public string strFinishtDate { get; set; }
            public string strHosName { get; set; }
            public string strCountedDays { get; set; }
            public string strDisease { get; set; }

        }

        public class DetailData
        {
            public string strShinryoBunrui { get; set; }
            public string strKomoku { get; set; }
            public string strScore { get; set; }
            public string strAmount { get; set; }
            public string strScoreSum { get; set; }
            public string strHMS { get; set; }
          
        }

        public class Biko
        {
            public string strBiko { get; set; }
        }
    }
}

﻿namespace ReadDoc
{
    partial class frmInput_forexcel
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_detail = new System.Windows.Forms.DataGridView();
            this.textBoxKoban = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxBranch = new System.Windows.Forms.TextBox();
            this.textBoxRezeCount = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxStart = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxFinish = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxPageForRezept = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxCreator = new System.Windows.Forms.TextBox();
            this.textBoxDisease = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxPatientBirthday = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxPatientName = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxHihoName = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxCountedDays = new System.Windows.Forms.TextBox();
            this.textBoxHosName = new System.Windows.Forms.TextBox();
            this.textBoxShinryoYM = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBoxHihoNumber = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxHihoMark = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxShibu = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxBiko = new System.Windows.Forms.TextBox();
            this.splitContainerCreate = new System.Windows.Forms.SplitContainer();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.buttonUpd = new System.Windows.Forms.Button();
            this.checkBoxHide = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_detail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerCreate)).BeginInit();
            this.splitContainerCreate.Panel1.SuspendLayout();
            this.splitContainerCreate.Panel2.SuspendLayout();
            this.splitContainerCreate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv_detail
            // 
            this.dgv_detail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_detail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_detail.Location = new System.Drawing.Point(0, 0);
            this.dgv_detail.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.dgv_detail.Name = "dgv_detail";
            this.dgv_detail.Size = new System.Drawing.Size(924, 369);
            this.dgv_detail.TabIndex = 0;
            this.dgv_detail.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgv_detail_EditingControlShowing);
            this.dgv_detail.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgv_detail_KeyDown);
            // 
            // textBoxKoban
            // 
            this.textBoxKoban.Location = new System.Drawing.Point(73, 9);
            this.textBoxKoban.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxKoban.Name = "textBoxKoban";
            this.textBoxKoban.Size = new System.Drawing.Size(144, 23);
            this.textBoxKoban.TabIndex = 0;
            this.textBoxKoban.Text = "1402040051A";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("メイリオ", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(24, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 23);
            this.label1.TabIndex = 2;
            this.label1.Text = "項番";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("メイリオ", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(404, 141);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 23);
            this.label2.TabIndex = 4;
            this.label2.Text = "開始";
            // 
            // textBoxBranch
            // 
            this.textBoxBranch.Location = new System.Drawing.Point(273, 9);
            this.textBoxBranch.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxBranch.Name = "textBoxBranch";
            this.textBoxBranch.Size = new System.Drawing.Size(48, 23);
            this.textBoxBranch.TabIndex = 1;
            this.textBoxBranch.Text = "10";
            // 
            // textBoxRezeCount
            // 
            this.textBoxRezeCount.Location = new System.Drawing.Point(415, 9);
            this.textBoxRezeCount.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxRezeCount.Name = "textBoxRezeCount";
            this.textBoxRezeCount.Size = new System.Drawing.Size(48, 23);
            this.textBoxRezeCount.TabIndex = 2;
            this.textBoxRezeCount.Text = "10";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("メイリオ", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(339, 9);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 23);
            this.label3.TabIndex = 4;
            this.label3.Text = "委託枚数";
            // 
            // textBoxStart
            // 
            this.textBoxStart.Location = new System.Drawing.Point(451, 141);
            this.textBoxStart.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxStart.Name = "textBoxStart";
            this.textBoxStart.Size = new System.Drawing.Size(163, 23);
            this.textBoxStart.TabIndex = 12;
            this.textBoxStart.Text = "令和02年10月30日";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("メイリオ", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(325, 141);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 23);
            this.label4.TabIndex = 4;
            this.label4.Text = "診療期間";
            // 
            // textBoxFinish
            // 
            this.textBoxFinish.Location = new System.Drawing.Point(670, 141);
            this.textBoxFinish.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxFinish.Name = "textBoxFinish";
            this.textBoxFinish.Size = new System.Drawing.Size(147, 23);
            this.textBoxFinish.TabIndex = 13;
            this.textBoxFinish.Text = "50210";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("メイリオ", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(624, 141);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 23);
            this.label5.TabIndex = 4;
            this.label5.Text = "終了";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("メイリオ", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(229, 9);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 23);
            this.label6.TabIndex = 4;
            this.label6.Text = "枝番";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.Location = new System.Drawing.Point(723, 8);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(113, 24);
            this.label7.TabIndex = 4;
            this.label7.Text = "当該申請書に係る\r\n総レセプト作成件数";
            // 
            // textBoxPageForRezept
            // 
            this.textBoxPageForRezept.Location = new System.Drawing.Point(844, 9);
            this.textBoxPageForRezept.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxPageForRezept.Name = "textBoxPageForRezept";
            this.textBoxPageForRezept.Size = new System.Drawing.Size(49, 23);
            this.textBoxPageForRezept.TabIndex = 4;
            this.textBoxPageForRezept.Text = "1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("メイリオ", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label8.Location = new System.Drawing.Point(490, 9);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 23);
            this.label8.TabIndex = 4;
            this.label8.Text = "作成者";
            // 
            // textBoxCreator
            // 
            this.textBoxCreator.Location = new System.Drawing.Point(553, 9);
            this.textBoxCreator.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxCreator.Name = "textBoxCreator";
            this.textBoxCreator.Size = new System.Drawing.Size(162, 23);
            this.textBoxCreator.TabIndex = 3;
            this.textBoxCreator.Text = "徳川慶喜";
            // 
            // textBoxDisease
            // 
            this.textBoxDisease.Location = new System.Drawing.Point(88, 206);
            this.textBoxDisease.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxDisease.Multiline = true;
            this.textBoxDisease.Name = "textBoxDisease";
            this.textBoxDisease.Size = new System.Drawing.Size(729, 56);
            this.textBoxDisease.TabIndex = 16;
            this.textBoxDisease.Text = "徳川慶喜";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("メイリオ", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label15.Location = new System.Drawing.Point(25, 223);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 23);
            this.label15.TabIndex = 4;
            this.label15.Text = "傷病名";
            // 
            // textBoxPatientBirthday
            // 
            this.textBoxPatientBirthday.Location = new System.Drawing.Point(452, 101);
            this.textBoxPatientBirthday.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxPatientBirthday.Name = "textBoxPatientBirthday";
            this.textBoxPatientBirthday.Size = new System.Drawing.Size(162, 23);
            this.textBoxPatientBirthday.TabIndex = 11;
            this.textBoxPatientBirthday.Text = "令和02年03月11日";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("メイリオ", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label14.Location = new System.Drawing.Point(325, 101);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(115, 23);
            this.label14.TabIndex = 4;
            this.label14.Text = "療養者生年月日";
            // 
            // textBoxPatientName
            // 
            this.textBoxPatientName.Location = new System.Drawing.Point(452, 74);
            this.textBoxPatientName.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxPatientName.Name = "textBoxPatientName";
            this.textBoxPatientName.Size = new System.Drawing.Size(441, 23);
            this.textBoxPatientName.TabIndex = 10;
            this.textBoxPatientName.Text = "徳川好子";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("メイリオ", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label13.Location = new System.Drawing.Point(325, 74);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(85, 23);
            this.label13.TabIndex = 4;
            this.label13.Text = "療養者氏名";
            // 
            // textBoxHihoName
            // 
            this.textBoxHihoName.Location = new System.Drawing.Point(452, 38);
            this.textBoxHihoName.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxHihoName.Name = "textBoxHihoName";
            this.textBoxHihoName.Size = new System.Drawing.Size(441, 23);
            this.textBoxHihoName.TabIndex = 9;
            this.textBoxHihoName.Text = "徳川慶喜";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("メイリオ", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label12.Location = new System.Drawing.Point(325, 38);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(85, 23);
            this.label12.TabIndex = 4;
            this.label12.Text = "組合員氏名";
            // 
            // textBoxCountedDays
            // 
            this.textBoxCountedDays.Location = new System.Drawing.Point(768, 177);
            this.textBoxCountedDays.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxCountedDays.Name = "textBoxCountedDays";
            this.textBoxCountedDays.Size = new System.Drawing.Size(49, 23);
            this.textBoxCountedDays.TabIndex = 15;
            this.textBoxCountedDays.Text = "10";
            // 
            // textBoxHosName
            // 
            this.textBoxHosName.Location = new System.Drawing.Point(161, 177);
            this.textBoxHosName.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxHosName.Name = "textBoxHosName";
            this.textBoxHosName.Size = new System.Drawing.Size(541, 23);
            this.textBoxHosName.TabIndex = 14;
            this.textBoxHosName.Text = "あいうえおかきくけこさしすせそ総合病院";
            this.textBoxHosName.Enter += new System.EventHandler(this.textBoxHosName_Enter);
            // 
            // textBoxShinryoYM
            // 
            this.textBoxShinryoYM.Location = new System.Drawing.Point(146, 141);
            this.textBoxShinryoYM.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxShinryoYM.Name = "textBoxShinryoYM";
            this.textBoxShinryoYM.Size = new System.Drawing.Size(162, 23);
            this.textBoxShinryoYM.TabIndex = 8;
            this.textBoxShinryoYM.Text = "令和02年01月";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("メイリオ", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label18.Location = new System.Drawing.Point(721, 177);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(40, 23);
            this.label18.TabIndex = 4;
            this.label18.Text = "日数";
            // 
            // textBoxHihoNumber
            // 
            this.textBoxHihoNumber.Location = new System.Drawing.Point(146, 101);
            this.textBoxHihoNumber.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxHihoNumber.Name = "textBoxHihoNumber";
            this.textBoxHihoNumber.Size = new System.Drawing.Size(162, 23);
            this.textBoxHihoNumber.TabIndex = 7;
            this.textBoxHihoNumber.Text = "1234567890";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("メイリオ", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label17.Location = new System.Drawing.Point(25, 177);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(115, 23);
            this.label17.TabIndex = 4;
            this.label17.Text = "受診医療機関名";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("メイリオ", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label16.Location = new System.Drawing.Point(24, 141);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(70, 23);
            this.label16.TabIndex = 4;
            this.label16.Text = "診療年月";
            // 
            // textBoxHihoMark
            // 
            this.textBoxHihoMark.Location = new System.Drawing.Point(146, 74);
            this.textBoxHihoMark.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxHihoMark.Name = "textBoxHihoMark";
            this.textBoxHihoMark.Size = new System.Drawing.Size(162, 23);
            this.textBoxHihoMark.TabIndex = 6;
            this.textBoxHihoMark.Text = "あいうえお";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("メイリオ", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label11.Location = new System.Drawing.Point(24, 101);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(115, 23);
            this.label11.TabIndex = 4;
            this.label11.Text = "被保険者証番号";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("メイリオ", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label10.Location = new System.Drawing.Point(24, 74);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(115, 23);
            this.label10.TabIndex = 4;
            this.label10.Text = "被保険者証記号";
            // 
            // textBoxShibu
            // 
            this.textBoxShibu.Location = new System.Drawing.Point(146, 38);
            this.textBoxShibu.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBoxShibu.Name = "textBoxShibu";
            this.textBoxShibu.Size = new System.Drawing.Size(162, 23);
            this.textBoxShibu.TabIndex = 5;
            this.textBoxShibu.Text = "神奈川";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("メイリオ", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label9.Location = new System.Drawing.Point(24, 38);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 23);
            this.label9.TabIndex = 4;
            this.label9.Text = "支部";
            // 
            // textBoxBiko
            // 
            this.textBoxBiko.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxBiko.Location = new System.Drawing.Point(0, 0);
            this.textBoxBiko.Multiline = true;
            this.textBoxBiko.Name = "textBoxBiko";
            this.textBoxBiko.Size = new System.Drawing.Size(924, 93);
            this.textBoxBiko.TabIndex = 0;
            // 
            // splitContainerCreate
            // 
            this.splitContainerCreate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerCreate.Location = new System.Drawing.Point(0, 0);
            this.splitContainerCreate.Name = "splitContainerCreate";
            this.splitContainerCreate.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerCreate.Panel1
            // 
            this.splitContainerCreate.Panel1.Controls.Add(this.dgv_detail);
            // 
            // splitContainerCreate.Panel2
            // 
            this.splitContainerCreate.Panel2.Controls.Add(this.textBoxBiko);
            this.splitContainerCreate.Size = new System.Drawing.Size(924, 472);
            this.splitContainerCreate.SplitterDistance = 369;
            this.splitContainerCreate.SplitterWidth = 10;
            this.splitContainerCreate.TabIndex = 0;
            // 
            // splitContainer
            // 
            this.splitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer.Location = new System.Drawing.Point(12, 36);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.textBoxDisease);
            this.splitContainer.Panel1.Controls.Add(this.label1);
            this.splitContainer.Panel1.Controls.Add(this.label9);
            this.splitContainer.Panel1.Controls.Add(this.textBoxKoban);
            this.splitContainer.Panel1.Controls.Add(this.label4);
            this.splitContainer.Panel1.Controls.Add(this.label7);
            this.splitContainer.Panel1.Controls.Add(this.label3);
            this.splitContainer.Panel1.Controls.Add(this.textBoxShibu);
            this.splitContainer.Panel1.Controls.Add(this.label6);
            this.splitContainer.Panel1.Controls.Add(this.textBoxBranch);
            this.splitContainer.Panel1.Controls.Add(this.label15);
            this.splitContainer.Panel1.Controls.Add(this.label8);
            this.splitContainer.Panel1.Controls.Add(this.textBoxPageForRezept);
            this.splitContainer.Panel1.Controls.Add(this.label10);
            this.splitContainer.Panel1.Controls.Add(this.textBoxCreator);
            this.splitContainer.Panel1.Controls.Add(this.label5);
            this.splitContainer.Panel1.Controls.Add(this.textBoxRezeCount);
            this.splitContainer.Panel1.Controls.Add(this.textBoxPatientBirthday);
            this.splitContainer.Panel1.Controls.Add(this.textBoxHihoNumber);
            this.splitContainer.Panel1.Controls.Add(this.textBoxHosName);
            this.splitContainer.Panel1.Controls.Add(this.textBoxHihoName);
            this.splitContainer.Panel1.Controls.Add(this.label11);
            this.splitContainer.Panel1.Controls.Add(this.label12);
            this.splitContainer.Panel1.Controls.Add(this.textBoxFinish);
            this.splitContainer.Panel1.Controls.Add(this.label17);
            this.splitContainer.Panel1.Controls.Add(this.label14);
            this.splitContainer.Panel1.Controls.Add(this.label18);
            this.splitContainer.Panel1.Controls.Add(this.textBoxStart);
            this.splitContainer.Panel1.Controls.Add(this.label13);
            this.splitContainer.Panel1.Controls.Add(this.textBoxHihoMark);
            this.splitContainer.Panel1.Controls.Add(this.label2);
            this.splitContainer.Panel1.Controls.Add(this.textBoxCountedDays);
            this.splitContainer.Panel1.Controls.Add(this.label16);
            this.splitContainer.Panel1.Controls.Add(this.textBoxPatientName);
            this.splitContainer.Panel1.Controls.Add(this.textBoxShinryoYM);
            this.splitContainer.Panel1.Font = new System.Drawing.Font("ＭＳ ゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.splitContainerCreate);
            this.splitContainer.Size = new System.Drawing.Size(924, 758);
            this.splitContainer.SplitterDistance = 276;
            this.splitContainer.SplitterWidth = 10;
            this.splitContainer.TabIndex = 2;
            // 
            // buttonUpd
            // 
            this.buttonUpd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonUpd.Location = new System.Drawing.Point(803, 800);
            this.buttonUpd.Name = "buttonUpd";
            this.buttonUpd.Size = new System.Drawing.Size(126, 31);
            this.buttonUpd.TabIndex = 10;
            this.buttonUpd.Text = "更新";
            this.buttonUpd.UseVisualStyleBackColor = true;
            // 
            // checkBoxHide
            // 
            this.checkBoxHide.AutoSize = true;
            this.checkBoxHide.Location = new System.Drawing.Point(12, 12);
            this.checkBoxHide.Name = "checkBoxHide";
            this.checkBoxHide.Size = new System.Drawing.Size(90, 19);
            this.checkBoxHide.TabIndex = 5;
            this.checkBoxHide.Text = "上部隠す";
            this.checkBoxHide.UseVisualStyleBackColor = true;
            this.checkBoxHide.CheckedChanged += new System.EventHandler(this.checkBoxHide_CheckedChanged);
            // 
            // frmInput_forexcel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(948, 835);
            this.Controls.Add(this.checkBoxHide);
            this.Controls.Add(this.buttonUpd);
            this.Controls.Add(this.splitContainer);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "frmInput_forexcel";
            this.Text = "作成画面";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_detail)).EndInit();
            this.splitContainerCreate.Panel1.ResumeLayout(false);
            this.splitContainerCreate.Panel2.ResumeLayout(false);
            this.splitContainerCreate.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerCreate)).EndInit();
            this.splitContainerCreate.ResumeLayout(false);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel1.PerformLayout();
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_detail;
        private System.Windows.Forms.TextBox textBoxKoban;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxBranch;
        private System.Windows.Forms.TextBox textBoxRezeCount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxStart;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxFinish;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxPageForRezept;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxCreator;
        private System.Windows.Forms.TextBox textBoxDisease;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxPatientBirthday;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBoxPatientName;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxHihoName;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxCountedDays;
        private System.Windows.Forms.TextBox textBoxHosName;
        private System.Windows.Forms.TextBox textBoxShinryoYM;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBoxHihoNumber;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBoxHihoMark;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxShibu;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxBiko;
        private System.Windows.Forms.SplitContainer splitContainerCreate;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.Button buttonUpd;
        private System.Windows.Forms.CheckBox checkBoxHide;
    }
}


﻿namespace ReadDoc
{
    partial class frmSearch
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSearch));
            this.dgv = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNo = new System.Windows.Forms.TextBox();
            this.btnFind = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbKigo = new System.Windows.Forms.ComboBox();
            this.chkWord = new System.Windows.Forms.CheckBox();
            this.chkExcel = new System.Windows.Forms.CheckBox();
            this.selectTab = new System.Windows.Forms.TabControl();
            this.tp1 = new System.Windows.Forms.TabPage();
            this.buttonImp = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.dtpVisible = new System.Windows.Forms.DateTimePicker();
            this.tp2 = new System.Windows.Forms.TabPage();
            this.chkWordKE = new System.Windows.Forms.CheckBox();
            this.chkPDF = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtKENo = new System.Windows.Forms.TextBox();
            this.tp3 = new System.Windows.Forms.TabPage();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dtpVisible3 = new System.Windows.Forms.DateTimePicker();
            this.cmbYM = new System.Windows.Forms.ComboBox();
            this.mtxtBirth = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbl = new System.Windows.Forms.Label();
            this.cmbCountry = new System.Windows.Forms.ComboBox();
            this.btnImp = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txthnum = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tssl = new System.Windows.Forms.ToolStripStatusLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.selectTab.SuspendLayout();
            this.tp1.SuspendLayout();
            this.tp2.SuspendLayout();
            this.tp3.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Location = new System.Drawing.Point(16, 154);
            this.dgv.Margin = new System.Windows.Forms.Padding(4);
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowTemplate.Height = 21;
            this.dgv.Size = new System.Drawing.Size(1227, 506);
            this.dgv.TabIndex = 0;
            this.dgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellContentClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(7, 37);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 23);
            this.label1.TabIndex = 2;
            this.label1.Text = "被保険者証番号";
            // 
            // txtNo
            // 
            this.txtNo.Location = new System.Drawing.Point(164, 34);
            this.txtNo.Margin = new System.Windows.Forms.Padding(4);
            this.txtNo.Name = "txtNo";
            this.txtNo.Size = new System.Drawing.Size(275, 30);
            this.txtNo.TabIndex = 3;
            this.txtNo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_KeyUp);
            this.txtNo.Validated += new System.EventHandler(this.txt_Validated);
            // 
            // btnFind
            // 
            this.btnFind.Font = new System.Drawing.Font("メイリオ", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnFind.Location = new System.Drawing.Point(1037, 65);
            this.btnFind.Margin = new System.Windows.Forms.Padding(4);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(107, 44);
            this.btnFind.TabIndex = 4;
            this.btnFind.Text = "検索";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(7, 68);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 23);
            this.label2.TabIndex = 5;
            this.label2.Text = "被保険者証記号";
            // 
            // cmbKigo
            // 
            this.cmbKigo.FormattingEnabled = true;
            this.cmbKigo.Location = new System.Drawing.Point(164, 65);
            this.cmbKigo.Margin = new System.Windows.Forms.Padding(4);
            this.cmbKigo.Name = "cmbKigo";
            this.cmbKigo.Size = new System.Drawing.Size(235, 31);
            this.cmbKigo.TabIndex = 7;
            this.cmbKigo.SelectedIndexChanged += new System.EventHandler(this.cmbKigo_SelectedIndexChanged);
            // 
            // chkWord
            // 
            this.chkWord.AutoSize = true;
            this.chkWord.Location = new System.Drawing.Point(6, 6);
            this.chkWord.Name = "chkWord";
            this.chkWord.Size = new System.Drawing.Size(67, 27);
            this.chkWord.TabIndex = 8;
            this.chkWord.Text = "Word";
            this.chkWord.UseVisualStyleBackColor = true;
            // 
            // chkExcel
            // 
            this.chkExcel.AutoSize = true;
            this.chkExcel.Checked = true;
            this.chkExcel.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkExcel.Location = new System.Drawing.Point(91, 6);
            this.chkExcel.Name = "chkExcel";
            this.chkExcel.Size = new System.Drawing.Size(67, 27);
            this.chkExcel.TabIndex = 9;
            this.chkExcel.Text = "Excel";
            this.chkExcel.UseVisualStyleBackColor = true;
            // 
            // selectTab
            // 
            this.selectTab.Controls.Add(this.tp1);
            this.selectTab.Controls.Add(this.tp2);
            this.selectTab.Controls.Add(this.tp3);
            this.selectTab.Font = new System.Drawing.Font("メイリオ", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.selectTab.Location = new System.Drawing.Point(16, 12);
            this.selectTab.Name = "selectTab";
            this.selectTab.SelectedIndex = 0;
            this.selectTab.Size = new System.Drawing.Size(996, 135);
            this.selectTab.TabIndex = 10;
            this.selectTab.SelectedIndexChanged += new System.EventHandler(this.selectTab_SelectedIndexChanged);
            // 
            // tp1
            // 
            this.tp1.BackColor = System.Drawing.Color.Transparent;
            this.tp1.Controls.Add(this.buttonImp);
            this.tp1.Controls.Add(this.label8);
            this.tp1.Controls.Add(this.chkWord);
            this.tp1.Controls.Add(this.dtpVisible);
            this.tp1.Controls.Add(this.chkExcel);
            this.tp1.Controls.Add(this.label1);
            this.tp1.Controls.Add(this.txtNo);
            this.tp1.Controls.Add(this.cmbKigo);
            this.tp1.Controls.Add(this.label2);
            this.tp1.Location = new System.Drawing.Point(4, 32);
            this.tp1.Name = "tp1";
            this.tp1.Padding = new System.Windows.Forms.Padding(3);
            this.tp1.Size = new System.Drawing.Size(988, 99);
            this.tp1.TabIndex = 0;
            this.tp1.Text = "協会健保";
            // 
            // buttonImp
            // 
            this.buttonImp.Location = new System.Drawing.Point(446, 31);
            this.buttonImp.Name = "buttonImp";
            this.buttonImp.Size = new System.Drawing.Size(148, 34);
            this.buttonImp.TabIndex = 14;
            this.buttonImp.Text = "テストインポート";
            this.buttonImp.UseVisualStyleBackColor = true;
            this.buttonImp.Click += new System.EventHandler(this.buttonImp_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(615, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(235, 23);
            this.label8.TabIndex = 13;
            this.label8.Text = "以下の日付より過去は検索しない";
            // 
            // dtpVisible
            // 
            this.dtpVisible.Location = new System.Drawing.Point(618, 42);
            this.dtpVisible.Name = "dtpVisible";
            this.dtpVisible.Size = new System.Drawing.Size(162, 30);
            this.dtpVisible.TabIndex = 12;
            // 
            // tp2
            // 
            this.tp2.BackColor = System.Drawing.Color.Transparent;
            this.tp2.Controls.Add(this.chkWordKE);
            this.tp2.Controls.Add(this.chkPDF);
            this.tp2.Controls.Add(this.label3);
            this.tp2.Controls.Add(this.txtKENo);
            this.tp2.Location = new System.Drawing.Point(4, 32);
            this.tp2.Name = "tp2";
            this.tp2.Padding = new System.Windows.Forms.Padding(3);
            this.tp2.Size = new System.Drawing.Size(988, 99);
            this.tp2.TabIndex = 1;
            this.tp2.Text = "公衆衛生";
            // 
            // chkWordKE
            // 
            this.chkWordKE.AutoSize = true;
            this.chkWordKE.Location = new System.Drawing.Point(477, 63);
            this.chkWordKE.Name = "chkWordKE";
            this.chkWordKE.Size = new System.Drawing.Size(112, 27);
            this.chkWordKE.TabIndex = 6;
            this.chkWordKE.Text = "Wordを表示";
            this.chkWordKE.UseVisualStyleBackColor = true;
            this.chkWordKE.CheckedChanged += new System.EventHandler(this.btnFind_Click);
            // 
            // chkPDF
            // 
            this.chkPDF.AutoSize = true;
            this.chkPDF.Location = new System.Drawing.Point(477, 34);
            this.chkPDF.Name = "chkPDF";
            this.chkPDF.Size = new System.Drawing.Size(103, 27);
            this.chkPDF.TabIndex = 6;
            this.chkPDF.Text = "PDFを表示";
            this.chkPDF.UseVisualStyleBackColor = true;
            this.chkPDF.CheckedChanged += new System.EventHandler(this.btnFind_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 23);
            this.label3.TabIndex = 5;
            this.label3.Text = "Kから始まる番号";
            // 
            // txtKENo
            // 
            this.txtKENo.Location = new System.Drawing.Point(176, 34);
            this.txtKENo.Margin = new System.Windows.Forms.Padding(4);
            this.txtKENo.MaxLength = 6;
            this.txtKENo.Name = "txtKENo";
            this.txtKENo.Size = new System.Drawing.Size(275, 30);
            this.txtKENo.TabIndex = 4;
            this.txtKENo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtKENo_KeyUp);
            // 
            // tp3
            // 
            this.tp3.BackColor = System.Drawing.SystemColors.Control;
            this.tp3.Controls.Add(this.label10);
            this.tp3.Controls.Add(this.label9);
            this.tp3.Controls.Add(this.dtpVisible3);
            this.tp3.Controls.Add(this.cmbYM);
            this.tp3.Controls.Add(this.mtxtBirth);
            this.tp3.Controls.Add(this.label7);
            this.tp3.Controls.Add(this.label5);
            this.tp3.Controls.Add(this.lbl);
            this.tp3.Controls.Add(this.cmbCountry);
            this.tp3.Controls.Add(this.btnImp);
            this.tp3.Controls.Add(this.label6);
            this.tp3.Controls.Add(this.label4);
            this.tp3.Controls.Add(this.txthnum);
            this.tp3.Location = new System.Drawing.Point(4, 32);
            this.tp3.Name = "tp3";
            this.tp3.Padding = new System.Windows.Forms.Padding(3);
            this.tp3.Size = new System.Drawing.Size(988, 99);
            this.tp3.TabIndex = 2;
            this.tp3.Text = "学校共済";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(606, 70);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 23);
            this.label10.TabIndex = 19;
            this.label10.Text = "↑未実装";
            this.label10.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(589, 12);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(310, 23);
            this.label9.TabIndex = 18;
            this.label9.Text = "診療年月　以下の日付より過去は検索しない";
            this.label9.Visible = false;
            // 
            // dtpVisible3
            // 
            this.dtpVisible3.Location = new System.Drawing.Point(592, 35);
            this.dtpVisible3.Name = "dtpVisible3";
            this.dtpVisible3.Size = new System.Drawing.Size(162, 30);
            this.dtpVisible3.TabIndex = 17;
            this.dtpVisible3.Visible = false;
            // 
            // cmbYM
            // 
            this.cmbYM.FormattingEnabled = true;
            this.cmbYM.Location = new System.Drawing.Point(453, 34);
            this.cmbYM.Name = "cmbYM";
            this.cmbYM.Size = new System.Drawing.Size(133, 31);
            this.cmbYM.TabIndex = 16;
            // 
            // mtxtBirth
            // 
            this.mtxtBirth.Location = new System.Drawing.Point(170, 37);
            this.mtxtBirth.Mask = "0000-00-00";
            this.mtxtBirth.Name = "mtxtBirth";
            this.mtxtBirth.Size = new System.Drawing.Size(128, 30);
            this.mtxtBirth.TabIndex = 15;
            this.mtxtBirth.ValidatingType = typeof(System.DateTime);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(376, 40);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 23);
            this.label7.TabIndex = 14;
            this.label7.Text = "診療年月";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label5.Location = new System.Drawing.Point(13, 37);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 23);
            this.label5.TabIndex = 14;
            this.label5.Text = "生年月日";
            // 
            // lbl
            // 
            this.lbl.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbl.Font = new System.Drawing.Font("ＭＳ ゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbl.Location = new System.Drawing.Point(828, 78);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(154, 15);
            this.lbl.TabIndex = 13;
            // 
            // cmbCountry
            // 
            this.cmbCountry.FormattingEnabled = true;
            this.cmbCountry.Location = new System.Drawing.Point(170, 67);
            this.cmbCountry.Name = "cmbCountry";
            this.cmbCountry.Size = new System.Drawing.Size(275, 31);
            this.cmbCountry.TabIndex = 12;
            // 
            // btnImp
            // 
            this.btnImp.Location = new System.Drawing.Point(833, 25);
            this.btnImp.Name = "btnImp";
            this.btnImp.Size = new System.Drawing.Size(137, 41);
            this.btnImp.TabIndex = 11;
            this.btnImp.Text = "取込テスト";
            this.btnImp.UseVisualStyleBackColor = true;
            this.btnImp.Click += new System.EventHandler(this.btnImp_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label6.Location = new System.Drawing.Point(13, 70);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 23);
            this.label6.TabIndex = 8;
            this.label6.Text = "国";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(13, 10);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 23);
            this.label4.TabIndex = 4;
            this.label4.Text = "被保険者証番号";
            // 
            // txthnum
            // 
            this.txthnum.Location = new System.Drawing.Point(170, 7);
            this.txthnum.Margin = new System.Windows.Forms.Padding(4);
            this.txthnum.Name = "txthnum";
            this.txthnum.Size = new System.Drawing.Size(275, 30);
            this.txthnum.TabIndex = 5;
            this.txthnum.TextChanged += new System.EventHandler(this.txthnum_TextChanged);
            this.txthnum.Validating += new System.ComponentModel.CancelEventHandler(this.txthnum_Validating);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tssl});
            this.statusStrip1.Location = new System.Drawing.Point(0, 678);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1259, 23);
            this.statusStrip1.TabIndex = 11;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tssl
            // 
            this.tssl.Name = "tssl";
            this.tssl.Size = new System.Drawing.Size(134, 18);
            this.tssl.Text = "toolStripStatusLabel1";
            // 
            // frmSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1259, 701);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.selectTab);
            this.Controls.Add(this.btnFind);
            this.Controls.Add(this.dgv);
            this.Font = new System.Drawing.Font("ＭＳ ゴシック", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmSearch";
            this.Text = "過去文書検索";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmSearch_FormClosed);
            this.Load += new System.EventHandler(this.frmSearch_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.selectTab.ResumeLayout(false);
            this.tp1.ResumeLayout(false);
            this.tp1.PerformLayout();
            this.tp2.ResumeLayout(false);
            this.tp2.PerformLayout();
            this.tp3.ResumeLayout(false);
            this.tp3.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNo;
        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbKigo;
        private System.Windows.Forms.CheckBox chkWord;
        private System.Windows.Forms.CheckBox chkExcel;
        private System.Windows.Forms.TabControl selectTab;
        private System.Windows.Forms.TabPage tp1;
        private System.Windows.Forms.TabPage tp2;
        private System.Windows.Forms.TextBox txtKENo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chkPDF;
        private System.Windows.Forms.CheckBox chkWordKE;
        private System.Windows.Forms.Button btnImp;
        private System.Windows.Forms.TabPage tp3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txthnum;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbCountry;
        private System.Windows.Forms.Label lbl;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.MaskedTextBox mtxtBirth;
        private System.Windows.Forms.ComboBox cmbYM;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tssl;
        private System.Windows.Forms.DateTimePicker dtpVisible;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dtpVisible3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button buttonImp;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Npgsql;

namespace ReadDoc
{
    public partial class frmWait : Form
    {
        public int pbmax = 0;
        public int pbmin = 0;
        public int intCnt;
        Npgsql.NpgsqlConnection cn;
        DataTable dt;
        public frmWait(Npgsql.NpgsqlConnection _cn,DataTable _dt)
        {
            InitializeComponent();
            cn = _cn;
            dt = _dt;
        }

        public void proceed()
        {
         
            pb.Maximum = dt.Rows.Count;
            pb.Minimum = 0;

            bw.RunWorkerAsync(100);

        }

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = (BackgroundWorker)sender;
            int bgworkerarg = (int)e.Argument;


            try
            {
                if (!entry(dt, worker))
                {
                    this.Close();
                }
                else
                {
                   
                    //個人情報部登録
                    DataTable tmpdt=CreateDTPersonal();
                    pb.Maximum = tmpdt.Rows.Count;

                    if (!entryPersonalData( worker, tmpdt))
                    {
                        
                        MessageBox.Show("個人情報部登録失敗");
                    }

                    //明細部登録
                    tmpdt = CreateDTDetail();
                    pb.Maximum = tmpdt.Rows.Count;
                    if (!entryRowData(tmpdt,worker))
                    {
                 
                        MessageBox.Show("明細部登録登録失敗");//明細部登録
                    };

                    //ファイル情報
                    if (!entryFileInfo())
                    {
                        
                        MessageBox.Show("明細部登録登録失敗");//ファイル情報
                    };

                  


                }
                e.Result = "OK";
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pb.Value = e.ProgressPercentage;
            lblcnt.Text = e.ProgressPercentage.ToString();
        }

        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Close();
        }


        /// <summary>
        /// データインポート
        /// </summary>
        /// <param name="lst"></param>
        /// <returns></returns>
        private bool entry(DataTable lst,BackgroundWorker w)
        {

            Npgsql.NpgsqlCommand cmd = new NpgsqlCommand();
            Npgsql.NpgsqlTransaction tran = cn.BeginTransaction();

            cmd.Connection = cn;
            int r = 0;

            try
            {                
                for (r = 0; r < lst.Rows.Count; r++)
                {
                    System.Text.StringBuilder sb = new StringBuilder();

                    sb.AppendFormat("insert into tblimport values('{0}','{1}',{2},'{3}','{4}',{5},'{6}','{7}','{8}')",
                        lst.Rows[r][0].ToString(),          //0fullpath		
                        lst.Rows[r][1].ToString(),          //1modifypath	
                        lst.Rows[r][2].ToString(),          //2datano	                                                                                   	
                        lst.Rows[r][3].ToString(),          //3datakey	              
                        lst.Rows[r][4].ToString(),          //4personaldata		
                        lst.Rows[r][5].ToString(),          //5rowno		                        	
                        lst.Rows[r][6].ToString(),          //6rowdata		
                        lst.Rows[r][7].ToString(),          //7createdate 	
                        lst.Rows[r][8].ToString()           //8updatedate 	
                        );

                    cmd.CommandText = sb.ToString();
                    cmd.ExecuteNonQuery();
                    w.ReportProgress(r);
                }

                tran.Commit();
            }
            catch (NpgsqlException nex)
            {
               // lbllog.Text="entry:" + lst.Rows[r][0].ToString() + nex.Message;
            }
            catch (Exception ex)
            {
                tran.Rollback();
                
            }
            finally
            {
               // cn.Close();
            }

            return true;
        }



        /// <summary>
        /// 個人情報部チェック。エラー時はデータキーと共に出力
        /// </summary>
        private void checkPersonalData()
        {
            Npgsql.NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.Connection = cn;
            DataTable chkdt = new DataTable();

            try
            {

                System.Text.StringBuilder sb = new StringBuilder();


                sb.AppendLine("  select ");
                sb.AppendLine(" datakey , ");                       //personalinfocd
                sb.AppendLine(" split_part(personaldata,',',12) sibu, ");//支部
                sb.AppendLine(" split_part(personaldata,',',17) hnum, ");//被保番
                sb.AppendLine(" split_part(personaldata,',',22) hname, ");//被保険者名
                sb.AppendLine(" split_part(personaldata,',',27) pname, ");//受療者
                sb.AppendLine(" split_part(personaldata,',',32) pbirth,");//受療者生年月日
                sb.AppendLine(" split_part(personaldata,',',19) ym,");//診療年月
                sb.AppendLine(" split_part(personaldata,',',34) country, ");//国名
                sb.AppendLine(" split_part(personaldata,',',42) dname, ");//傷病名
                sb.AppendLine(" split_part(personaldata,',',50) countdays, ");//診療実日数
                sb.AppendLine(" datano, ");//ファイル連番
                sb.AppendLine(" ''  ");//予備

                sb.AppendLine(" from tblimport ");

                sb.AppendLine(" group by  ");
                sb.AppendLine(" datakey , ");
                sb.AppendLine(" split_part(personaldata,',',12), ");
                sb.AppendLine(" split_part(personaldata,',',17), ");
                sb.AppendLine(" split_part(personaldata,',',22), ");
                sb.AppendLine(" split_part(personaldata,',',27), ");
                sb.AppendLine(" split_part(personaldata,',',32), ");
                sb.AppendLine(" split_part(personaldata,',',19), ");
                sb.AppendLine(" split_part(personaldata,',',34), ");
                sb.AppendLine(" split_part(personaldata,',',42), ");
                sb.AppendLine(" split_part(personaldata,',',50), ");
                sb.AppendLine(" datano");


                cmd.CommandText = sb.ToString();
                Npgsql.NpgsqlDataAdapter da = new NpgsqlDataAdapter(cmd);
                da.Fill(chkdt);
                pb.Maximum = chkdt.Rows.Count;

                for (int r = 0; r < chkdt.Rows.Count; r++)
                {
                    if (!int.TryParse(chkdt.Rows[r]["pbirth"].ToString(), out int tmppb))
                        lbllog.Text +="checkPersonalData:" + chkdt.Rows[r]["datakey"].ToString() + "受療者生年月日エラー" + '\n';

                    if (!int.TryParse(chkdt.Rows[r]["ym"].ToString(), out int tmpym))
                        lbllog.Text += "checkPersonalData:" + chkdt.Rows[r]["datakey"].ToString() + "診療年月エラー" + '\n';

                    if (!int.TryParse(chkdt.Rows[r]["countdays"].ToString(), out int tmpcd))
                        lbllog.Text += "checkPersonalData:" + chkdt.Rows[r]["datakey"].ToString() + "診療実日数エラー" + '\n';
                    
                }


            }
            catch (NpgsqlException nex)
            {
                lbllog.Text += "entryPersonalData:" + nex.Message + '\n';
            }

        }

        private void frmWait_Shown(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            proceed();
        }


        private DataTable CreateDTPersonal()
        {
            Npgsql.NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.Connection = cn;
            Npgsql.NpgsqlDataAdapter da = new NpgsqlDataAdapter();
            DataTable tmpdt = new DataTable();
            System.Text.StringBuilder sb = new StringBuilder();
            //一発挿入はエラーで落ちるのでforにする


            sb.Remove(0, sb.ToString().Length);
            sb.AppendLine("  select ");
            sb.AppendLine(" datakey , ");                       //personalinfocd
            sb.AppendLine(" split_part(personaldata,',',12) sibu, ");//支部
            sb.AppendLine(" split_part(personaldata,',',17) hihonum, ");//被保番
            sb.AppendLine(" split_part(personaldata,',',22) hihoname, ");//被保険者名
            sb.AppendLine(" split_part(personaldata,',',27) juryoname, ");//受療者

            //受療者生年月日
            sb.AppendLine(" cast( ");
            sb.AppendLine(" case  ");
            sb.AppendLine(" when split_part(personaldata,',',32) ~'^[0-9]{5}' then ");
            sb.AppendLine(" cast(cast('1900/01/01' as date) + cast(split_part(personaldata,',',32) as int) as varchar) ");
            sb.AppendLine(" else ");
            sb.AppendLine(" split_part(personaldata,',',32) ");
            sb.AppendLine(" end ");
            sb.AppendLine("  as date) juryobirthday, ");

            //診療年月
            sb.AppendLine(" case  ");
            sb.AppendLine(" when split_part(personaldata,',',19) ~'^[0-9]{5}' then ");
            sb.AppendLine(" substr(cast(cast('1900/01/01' as date) + cast(split_part(personaldata,',',19) as int) as varchar),1,7) ");
            sb.AppendLine(" else ");
            sb.AppendLine(" split_part(personaldata,',',19) ");
            sb.AppendLine(" end ym, ");

            sb.AppendLine(" split_part(personaldata,',',34) country, ");//国名
            sb.AppendLine(" split_part(personaldata,',',42) dname, ");//傷病名
            sb.AppendLine(" cast(replace(split_part(personaldata,',',50),'日','') as int) countday, ");//診療実日数
            sb.AppendLine(" datano, ");//ファイル連番
            sb.AppendLine(" ''  ");//予備

            sb.AppendLine(" from tblimport ");



            sb.AppendLine(" group by  ");
            sb.AppendLine(" datakey , ");
            sb.AppendLine(" split_part(personaldata,',',12), ");
            sb.AppendLine(" split_part(personaldata,',',17), ");
            sb.AppendLine(" split_part(personaldata,',',22), ");
            sb.AppendLine(" split_part(personaldata,',',27), ");
            sb.AppendLine(" split_part(personaldata,',',32), ");
            sb.AppendLine(" split_part(personaldata,',',19), ");
            sb.AppendLine(" split_part(personaldata,',',34), ");
            sb.AppendLine(" split_part(personaldata,',',42), ");
            sb.AppendLine(" split_part(personaldata,',',50), ");
            sb.AppendLine(" datano");

            cmd.CommandText = sb.ToString();
            da.SelectCommand = cmd;
            da.Fill(tmpdt);
            return tmpdt;
        }

        /// <summary>
        /// 個人情報部登録
        /// </summary>
        /// <returns></returns>
        private bool entryPersonalData(BackgroundWorker w,DataTable tmpdt)
        {

            Npgsql.NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.Connection = cn;
            Npgsql.NpgsqlDataAdapter da = new NpgsqlDataAdapter();
            
            
            try
            {

                System.Text.StringBuilder sb = new StringBuilder();
            
                int cnt=0;
                foreach (DataRow dr in tmpdt.Rows)
                {
                    sb.Remove(0, sb.ToString().Length);
                    sb.AppendLine(" insert into tblPerson Values (");

                    sb.AppendFormat("'{0}',", dr["datakey"].ToString());
                    sb.AppendFormat("'{0}',", dr["sibu"].ToString());
                    sb.AppendFormat("'{0}',", dr["hihonum"].ToString());
                    sb.AppendFormat("'{0}',", dr["hihoname"].ToString());
                    sb.AppendFormat("'{0}',", dr["juryoname"].ToString());
                    sb.AppendFormat("'{0}',", dr["juryobirthday"].ToString());
                    sb.AppendFormat("'{0}',", dr["ym"].ToString());
                    sb.AppendFormat("'{0}',", dr["country"].ToString());
                    sb.AppendFormat("'{0}',", dr["dname"].ToString());
                    sb.AppendFormat("{0},", dr["countday"].ToString());

                    sb.AppendFormat("'{0}',", dr["datano"].ToString());
                    sb.AppendFormat("'{0}')", string.Empty);


                    cmd.CommandText = sb.ToString();

                    try
                    {
                        cmd.ExecuteNonQuery();
                        w.ReportProgress(cnt);
                        cnt++;
                    }
                    catch (NpgsqlException nex)
                    {
                        
                        //lbllog.Text="entryPersonalData:" + dr["datakey"].ToString() + nex.Message;
                    }
                    catch (Exception ex)
                    {
                       // lbllog.Text="entryPersonalData:" + dr["datakey"].ToString() + ex.Message;
                    }
                }

            }
            catch (NpgsqlException nex)
            {
                //lbllog.Text="entryPersonalData:" + nex.Message;
            }
            catch (Exception ex)
            {
              //  lbllog.Text="entryPersonalData:" + ex.Message;
            }


            return true;
        }

        private DataTable CreateDTDetail()
        {
            Npgsql.NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.Connection = cn;
            Npgsql.NpgsqlDataAdapter da = new NpgsqlDataAdapter();
            DataTable tmpdt = new DataTable();
            System.Text.StringBuilder sb = new StringBuilder();
            //一発挿入はエラーで落ちるのでforにする

            sb.AppendLine("  select  ");
            sb.AppendLine("  datano,  ");                                      //ファイル番号
            sb.AppendLine("  datakey,  ");                                     // 個人情報管理番号
            sb.AppendLine("  rowno,   ");                                      // 行番号
            sb.AppendLine("  split_part(rowdata,',',1) bunrui,  ");            // 明細書の「点数表分類」
            sb.AppendLine("  split_part(rowdata,',',2) komoku,  ");            // 明細書の「項目」
            sb.AppendLine("  split_part(rowdata,',',3) tensu,   ");            // 明細書の「点数」
            sb.AppendLine("  split_part(rowdata,',',4) suryo,   ");            // 明細書の「数量」
            sb.AppendLine("  split_part(rowdata,',',5) hokentensu,   ");       // 明細書の「保険点数」
            sb.AppendLine("  createdate,  ");                                  // yyyyMMdd HHmmss　ファイル情報から取得
            sb.AppendLine("  updatedate	 ");                                   // yyyyMMdd HHmmss　ファイル情報から取得
            sb.AppendLine("  from tblimport  ");
            sb.AppendLine("  where   ");
            sb.AppendLine("  split_part(rowdata,',',3) not like '%合計%' and "); //点数(単価)、数量、点数(合計）が入っているレコードを抽出
            sb.AppendLine("  split_part(rowdata,',',4)<>''  and  ");
            sb.AppendLine("  split_part(rowdata,',',5)<>''   ");

            sb.AppendLine(" union all  ");

            sb.AppendLine("  select  ");
            sb.AppendLine("  datano,  ");                                      //ファイル番号
            sb.AppendLine("  datakey,  ");                                     // 個人情報管理番号
            sb.AppendLine("  rowno,   ");                                      // 行番号
            sb.AppendLine("  '' bunrui,  ");                                   // 明細書の「点数表分類」
            sb.AppendLine("  '' komoku,  ");                                   // 明細書の「項目」
            sb.AppendLine("  '合計(点)' tensu,   ");                           // 明細書の「点数」
            sb.AppendLine("  '' suryo,   ");                                   // 明細書の「数量」
            sb.AppendLine("  split_part(rowdata,',',5) hokentensu,   ");       // 明細書の「保険点数」
            sb.AppendLine("  createdate,  ");                                  // yyyyMMdd HHmmss　ファイル情報から取得
            sb.AppendLine("  updatedate	 ");                                   // yyyyMMdd HHmmss　ファイル情報から取得
            sb.AppendLine("  from tblimport  ");
            sb.AppendLine("  where   ");
            sb.AppendLine("  split_part(rowdata,',',3) like '%合計%' ");       //合計が入っているレコードを抽出

            sb.AppendLine(" union all  ");


            sb.AppendLine("  select  ");
            sb.AppendLine("  datano,  ");                                    //ファイル番号
            sb.AppendLine("  datakey,  ");                                   // 個人情報管理番号     
            sb.AppendLine("  rowno,   ");                                    // 行番号     
            sb.AppendLine("  '' bunrui,  ");                                 // 本来は明細書の「点数表分類」だが、備考だけレコードなので空欄にする
            sb.AppendLine("  split_part(rowdata,',',1) komoku,  ");          // 明細書の「項目」     に備考の文字列を入れる
            sb.AppendLine("  '' tensu,   ");                                 // 本来は明細書の「点数」     だが、備考だけレコードなので空欄にする
            sb.AppendLine("  '' suryo,   ");                                 // 本来は明細書の「数量」     だが、備考だけレコードなので空欄にする
            sb.AppendLine("  '' hokentensu,   ");                            // 本来は明細書の「保険点数」   だが、備考だけレコードなので空欄にする  
            sb.AppendLine("  createdate,  ");                                // yyyyMMdd HHmmss　ファイル情報から取得     
            sb.AppendLine("  updatedate	 ");                                 // yyyyMMdd HHmmss　ファイル情報から取得     
            sb.AppendLine("  from tblimport  ");
            sb.AppendLine("  where   ");
            sb.AppendLine("  rowdata like '%備考%'   ");                     //「備考」が入っているレコードを抽出

            sb.AppendLine("  group by datano,datakey,rowno,rowdata,createdate,updatedate	 ");

            sb.AppendLine("  order by datano,rowno");

            cmd.CommandText = sb.ToString();
            da.SelectCommand = cmd;
            da.Fill(tmpdt);
            return tmpdt;
        }

        /// <summary>
        /// 明細データ登録
        /// </summary>
        /// <returns></returns>
        private bool entryRowData(DataTable tmpdt,BackgroundWorker w)
        {
            Npgsql.NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.Connection = cn;
            Npgsql.NpgsqlDataAdapter da = new NpgsqlDataAdapter();
            int cnt = 0;

            try
            {
                System.Text.StringBuilder sb = new StringBuilder();
             
              
                foreach (DataRow dr in tmpdt.Rows)
                {
                    sb.Remove(0, sb.ToString().Length);
                    sb.AppendLine(" insert into tblData Values (");

                    sb.AppendFormat("'{0}',", dr["datano"].ToString());
                    sb.AppendFormat("'{0}',", dr["datakey"].ToString());
                    sb.AppendFormat("{0},", dr["rowno"].ToString());
                    sb.AppendFormat("'{0}',", dr["bunrui"].ToString());
                    sb.AppendFormat("'{0}',", dr["komoku"].ToString());
                    sb.AppendFormat("'{0}',", dr["tensu"].ToString());
                    sb.AppendFormat("'{0}',", dr["suryo"].ToString());
                    sb.AppendFormat("'{0}',", dr["hokentensu"].ToString());
                    sb.AppendFormat("'{0}',", dr["createdate"].ToString());
                    sb.AppendFormat("'{0}')", dr["updatedate"].ToString());

                    cmd.CommandText = sb.ToString();

                    try
                    {

                        cmd.ExecuteNonQuery();
                        w.ReportProgress(cnt);
                        cnt++;
                    }
                    catch (NpgsqlException nex)
                    {
                  //      lbllog.Text="entrRowData:" + dr["datakey"].ToString() + sb.ToString() + nex.Message;
                    }
                    catch (Exception ex)
                    {
                  //      lbllog.Text="entrRowData:" + dr["datakey"].ToString() + sb.ToString() + ex.Message;
                    }
                }



            }

            catch (NpgsqlException nex)
            {
           //     lbllog.Text="entryRowData:" + nex.Message;
                
            }
            catch (Exception ex)
            {
              //  lbllog.Text="entryRowData:" + ex.Message;
                
            }

            return true;
        }

        /// <summary>
        /// ファイル情報登録
        /// </summary>
        /// <returns></returns>
        private bool entryFileInfo()
        {

            Npgsql.NpgsqlCommand cmd = new NpgsqlCommand();
            cmd.Connection = cn;

            try
            {

                System.Text.StringBuilder sb = new StringBuilder();

                sb.AppendLine(" insert into tblfile  ");
                sb.AppendLine(" select ");
                sb.AppendLine(" fullpath, ");
                sb.AppendLine(" '', ");
                sb.AppendLine(" modifypath, ");
                sb.AppendLine(" '', ");
                sb.AppendLine(" datano, ");
                sb.AppendLine(" datakey ");

                sb.AppendLine(" from ");
                sb.AppendLine(" tblimport ");

                sb.AppendLine(" group by  ");
                sb.AppendLine(" fullpath, ");
                sb.AppendLine(" modifypath, ");
                sb.AppendLine(" datano, ");
                sb.AppendLine(" datakey ");

                cmd.CommandText = sb.ToString();
                cmd.ExecuteNonQuery();

            }
            catch (NpgsqlException nex)
            {
              //  lbllog.Text="entryFileInfo:" + nex.Message;
            }
            catch (Exception ex)
            {
              //  lbllog.Text="entryFileInfo:" + ex.Message;
            }

            return true;
        }


    }
}

option explicit
on error resume next

dim objWord,objDoc
dim args
dim fso
dim strFullFilePath

Const wdSeekCurrentPageHeader=9
Const wdAlignParagraphLeft=0

set args=Wscript.Arguments
set fso=CreateObject("scripting.FileSystemObject")

strFullFilePath=split(args(0),",")(0)

dim strExtension
strExtension=fso.GetExtensionName(strFullFilePath)'ファイルの拡張子を取得

'wordでない場合抜ける
if strExtension <>"docx" then 
    Wscript.Quit
    wscript.sleep 1500
end if


set objWord=CreateObject("word.application")
objWord.visible=true

set objDoc=objWord.Documents.open(strFullFilePath,,true)


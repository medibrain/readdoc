option explicit
on error resume next

dim objExcel,objWB,objWS
dim args
dim fso
dim strFullFilePath


set args=Wscript.Arguments
set fso=CreateObject("scripting.FileSystemObject")

strFullFilePath=split(args(0),",")(0)


dim strExtension
strExtension=fso.GetExtensionName(strFullFilePath)'ファイルの拡張子を取得

'pdfでない場合抜ける
if strExtension <>"pdf" and strExtension<>"PDF" then
    Wscript.Quit
    wscript.sleep 1000
end if


'純粋なファイル名だけを抽出
dim fn
fn=replace(fso.GetFileName(strFullFilePath),"." & strExtension,"")

dim objShell
set objShell=CreateObject("wscript.shell")



dim strCommand
objShell.currentdirectory= fso.getParentFolderName(WScript.ScriptFullName)
strCommand="SumatraPDF.exe """ & strFullFilePath & """"

objShell.run(strCommand)

if err.number>0 then msgbox "vbsOpenPDF" & vbcrlf & err.description

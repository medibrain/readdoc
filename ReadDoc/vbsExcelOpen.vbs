option explicit
on error resume next

dim objExcel,objWB,objWS
dim args
dim fso
dim strFullFilePath


set args=Wscript.Arguments
set fso=CreateObject("scripting.FileSystemObject")

'20191216101452 furukawa st ////////////////////////
'公衆衛生の場合、機密性の引数が不要だが受ける側が必要だったのでエラーになってた
'strKimitusei=split(args(0),",")(1)
'20191216101452 furukawa ed ////////////////////////


strFullFilePath=split(args(0),",")(0)

dim strExtension
strExtension=fso.GetExtensionName(strFullFilePath)'ファイルの拡張子を取得

'excelでない場合抜ける
if strExtension ="xlsx" and strExtension ="xls" then 
    Wscript.Quit
    wscript.sleep 1000
end if


'純粋なファイル名だけを抽出
dim fn
fn=replace(fso.GetFileName(strFullFilePath),"." & strExtension,"")


set objExcel=CreateObject("excel.application")
application.msoAutomationSecurityForceDisable=true
objExcel.visible=true
set objWB=objExcel.Workbooks.Open(strFullFilePath)
application.msoAutomationSecurityForceDisable=false




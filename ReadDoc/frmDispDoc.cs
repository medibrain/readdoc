﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ReadDoc
{
    public partial class frmDispDoc : Form
    {
        string strPath = string.Empty;
        
        public frmDispDoc(string _strPath)
        {
            InitializeComponent();
            strPath = _strPath;
            show();
        }

        private void show()
        {

            //object wd;
            //wd = Type.GetTypeFromProgID("Word.Application");
            //object dcs= wd.GetType().("Documents", 
            //    System.Reflection.BindingFlags.GetProperty, null, wd, new object[] { strPath });

            //object dc;
            //dc=dcs.GetType().InvokeMember("Open", System.Reflection.BindingFlags.InvokeMethod, null, dcs,new object[]{strPath});


            //Type t;
            //t = Type.GetTypeFromProgID("Word.Application");
            
            

            //dynamic dyn = Activator.CreateInstance(t);
            //dyn.Visible = true;

            //スクリプトが一番早い
            System.Diagnostics.ProcessStartInfo pi = new System.Diagnostics.ProcessStartInfo("wscript.exe");
            pi.Arguments = "vbsWordOpen.vbs " + strPath;

            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo = pi;
            p.Start();

            


            
            //System.IO.FileStream fs=new System.IO.FileStream(strPath,System.IO.FileMode.Open);
            
            //NPOI.XWPF.UserModel.XWPFDocument d = new NPOI.XWPF.UserModel.XWPFDocument(fs);
            //NPOI.XWPF.Extractor.XWPFWordExtractor e=new NPOI.XWPF.Extractor.XWPFWordExtractor(d);
           
            //string tmp = e.Text;
            //tmp=tmp.Replace("\n\n","\n");
            //this.rtx.Text=tmp;
            //d.Close();
            
        }

        private void txtSrch_Validated(object sender, EventArgs e)
        {
          


        }

        int pos = 0;
        private void srch()
        {

            pos = this.rtx.Find(txtSrch.Text, pos, RichTextBoxFinds.MatchCase);

            if (pos < 0)
            {
                MessageBox.Show("存在しません");
                pos = 0;

                return;
            }

            this.rtx.Select(pos, txtSrch.Text.Length);
            this.rtx.Focus();
            pos++;
        }
        private void btnSrch_Click(object sender, EventArgs e)
        {
            srch();
        }

        private void txtSrch_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtSrch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) srch();
        }
    }
}

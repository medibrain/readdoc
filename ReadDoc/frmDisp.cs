﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ReadDoc
{
    public partial class frmDisp : Form
    {
        string strpath = null;
        string strKoban = string.Empty;
        DataTable dtKK = new DataTable();

        //20210310143626 furukawa st ////////////////////////
        //項番を引き継いで、DBから取れるように
        
        public frmDisp(string _strpath,DataTable _dtKK)
        //      public frmDisp(string _strpath, string _strKoban)
        //20210310143626 furukawa ed ////////////////////////

        {
            InitializeComponent();
            strpath = _strpath;

            //20210310143653 furukawa st ////////////////////////
            //項番を引き継いで、DBから取れるように

            dtKK = _dtKK;
            //20210310143653 furukawa ed ////////////////////////


            this.Text = "閲覧:" + System.IO.Path.GetFileName(strpath);
        }


        //セル値の取得
        private string retVal(NPOI.SS.UserModel.ICell cell)
        {
            if (cell == null) return string.Empty;

            switch (cell.CellType.ToString())
            {
                case "Text":
                case "String":
                    return cell.StringCellValue.Trim();
                
                case "Numeric":
                    return cell.NumericCellValue.ToString().Trim();
             
                default:
                    return string.Empty;
            }
        }



        private bool LoadNPOI()
        {
            //グリッドスタイル
            DataGridViewCellStyle cs = new DataGridViewCellStyle();            
            cs.Font = new System.Drawing.Font("ＭＳ ゴシック", 11);
            
            //行数
            int row = 0;
            try
            {
                //strpath = "C:\\user_furukawa\\60試験データ\\協会けんぽ\\テスト\\フォーマット試験\\test4\\071_Ax2_Bx18\\143010-013-1A-2ｍ\\143010-013-1A-1.xlsx";
                NPOI.SS.UserModel.IWorkbook w = NPOI.SS.UserModel.WorkbookFactory.Create(strpath);

                if (w.NumberOfSheets < 2)
                {
                    MessageBox.Show("別紙2がありません",Application.ProductName,MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                    return false;
                }

                
                NPOI.SS.UserModel.ISheet s = w.GetSheetAt(1);
                NPOI.SS.UserModel.IRow ir = null;


                #region 個人情報部分


                DataTable dp = new DataTable();
                dp.Columns.Add();
                dp.Columns.Add();
                dp.Columns.Add();
                dp.Columns.Add();
                dp.Columns.Add();
                dp.Columns.Add();

                
                NPOI.SS.Util.CellRangeAddress rng = new NPOI.SS.Util.CellRangeAddress(4,13,1,6);
                for (int r = rng.FirstRow; r < rng.LastRow; r++)
                {
                    DataRow dr = dp.NewRow();
                    
                    for(int c=rng.FirstColumn;c<rng.LastColumn;c++)
                    {
                        NPOI.SS.UserModel.ICell cc=s.GetRow(r).GetCell(c);
                      
                        dr[c] = retVal(cc);
                    }
                    dp.Rows.Add(dr);
                }
                this.dgvP.DataSource = dp;
                this.dgvP.Columns[0].Width = 3;
                this.dgvP.Columns[1].Width = 100;
                this.dgvP.Columns[2].Width = 150;
                this.dgvP.Columns[3].Width = 3;
                this.dgvP.Columns[4].Width = 150;
                this.dgvP.Columns[5].Width = 150;
                this.dgvP.ColumnHeadersVisible = false;


                this.dgvP.DefaultCellStyle = cs;
                this.dgvP.Columns[1].DefaultCellStyle.BackColor = Color.LightGray;
                this.dgvP.Columns[4].DefaultCellStyle.BackColor = Color.LightGray;
                #endregion

                #region 管理部分用
                DataTable dtk = new DataTable();
                dtk.Columns.Add();
                dtk.Columns.Add();
                dtk.Columns.Add();
                dtk.Columns.Add();
                dtk.Columns.Add();
                dtk.Columns.Add();
             

                NPOI.SS.Util.CellRangeAddress rngK = new NPOI.SS.Util.CellRangeAddress(1, 9, 8, 13);
                for (int r = rngK.FirstRow; r < rngK.LastRow; r++)
                {
                    DataRow drk = dtk.NewRow();
                    if (s.GetRow(r) == null) continue;

                    for (int c = rngK.FirstColumn; c < rngK.LastColumn; c++)
                    {
                        NPOI.SS.UserModel.ICell cc = s.GetRow(r).GetCell(c);

                        drk[c-8] = retVal(cc);
                    }
                    dtk.Rows.Add(drk);
                }
                this.dgvK.DataSource = dtk;
                this.dgvK.Columns[0].Width = 100;
                this.dgvK.Columns[1].Width = 100;
                this.dgvK.Columns[2].Width = 150;
                this.dgvK.Columns[3].Width = 53;
                this.dgvK.Columns[4].Width = 150;
                this.dgvK.ColumnHeadersVisible = false;

                this.dgvK.DefaultCellStyle = cs;
                #endregion

                #region データ部
                DataTable disp = new DataTable();
                disp.Columns.Add("診療識別");
                disp.Columns.Add("項目");
                disp.Columns.Add("単価");
                disp.Columns.Add("数量");
                disp.Columns.Add("点数");
                disp.Columns.Add("ほみそ");
                //disp.Columns.Add();
                //disp.Columns.Add();
                //disp.Columns.Add();
                //disp.Columns.Add();


                string strbiko = string.Empty;

                //データは15行目以降、最大300行
                for (row = 15; row < 300; row++)
                {
                    //if (row >= 53 && row <= 63) continue;
                    
                    
                    ir = s.GetRow(row);
                    DataRow dr = disp.NewRow();

                    bool flg = false;
                    if (ir == null) continue;
                    
                    //行の各セルの検証
                    for (int c = 0; c < ir.Cells.Count; c++)
                    {
                        //DataTableの項目数より多い場合は飛ばす
                        if (disp.Columns.Count <= c) break;
                        //項目数が2未満はとばす
                        if (ir.Cells.Count < 2) break;
                                               
                        switch (ir.Cells[c].CellType.ToString())
                        {
                            case "Formula":
                                dr[c] = ir.Cells[c].NumericCellValue;
                                break;

                            case "Numeric":
                                dr[c] = ir.Cells[c].NumericCellValue;
                                break;

                            default:
                                dr[c] = ir.Cells[c];
                                break;
                        }
                        
                        
                    }
                    //見出しは飛ばす
                    if (dr[0].ToString() == "点数表分類")  flg = true; 

                    //項目、点数、数量ともに0の場合は表示させない
                    if (dr[1].ToString() == "" && 
                        dr[2].ToString() == "" && 
                        dr[3].ToString() == "") flg = true;

                    //flgがfalseの場合のみ登録
                    if (!flg) disp.Rows.Add(dr);

                    //備考取得
                    if (dr[0].ToString().IndexOf("備考")>0 && dr[0].ToString().Length>6)
                    {
                        strbiko = dr[0].ToString().Replace('　','\0');
                        txtBiko.Text = strbiko;
                    }
                }

                this.dgv.DataSource = disp;
                this.dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

                dgv.DefaultCellStyle = cs;
                dgv.Columns[4].DefaultCellStyle.BackColor = Color.LightGray;
                #endregion

                #region 備考シート
                if (w.NumberOfSheets >= 3)
                {
                    s = w.GetSheetAt(2);
                    if (s.SheetName != "備考") return true;


                    //20191212150709 furukawa st ////////////////////////
                    //文字を読み取れてないので1番地からに変更
                    
                    rng = new NPOI.SS.Util.CellRangeAddress(1, 53, 1,22);
                                //rng = new NPOI.SS.Util.CellRangeAddress(7, 53, 3, 22);
                    //20191212150709 furukawa ed ////////////////////////


                    for (int r = rng.FirstRow; r < rng.LastRow; r++)
                    {
                       // DataRow dr = dp.NewRow();

                        for (int c = rng.FirstColumn; c < rng.LastColumn; c++)
                        {
                            NPOI.SS.UserModel.ICell cc = s.GetRow(r).GetCell(c);
                            
                            strbiko += retVal(cc);
                        }
                        //  dp.Rows.Add(dr);
                        strbiko += "\r\n";
                    }

                }

                //20191212150831 furukawa st ////////////////////////
                //備考シートのない時期の備考欄の文字列も繋いで出す
                
                if (strbiko != string.Empty) txtBiko.Text += strbiko.Trim();
                            //if (strbiko != string.Empty) txtBiko.Text += "\r\n" + strbiko.Trim();
                //20191212150831 furukawa ed ////////////////////////


                #endregion


                return true;
            }
            catch(NPOI.UnsupportedFileFormatException uffex)
            {
                MessageBox.Show(row + uffex.Message);
                return false;
            }
            catch(NPOI.POIXMLException xmlex)
            {
                MessageBox.Show(row + xmlex.Message);
                return false;
            }
            catch(Exception ex)
            {
                MessageBox.Show(row + ex.Message);
                return false;
            }

        }


        private void frm_Load(object sender, EventArgs e)
        {
            if (!LoadNPOI()) this.Close() ;
            return;
        }


        private Control ctlClick;

        #region コピー文字化け対策
        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Control c = ActiveControl;
            if (ctlClick != null)
            {
                c = ctlClick;
            }
            
    
            if(c.GetType().Equals(typeof(DataGridView)))
            {
                DataGridView tmpdgv = new DataGridView() ;
                tmpdgv = (DataGridView)c;
                Clipboard.SetDataObject(tmpdgv.GetClipboardContent());
                Clipboard.SetDataObject(Clipboard.GetData(DataFormats.Text));
            }
     
        }



        #endregion

        private void cms_Opening(object sender, CancelEventArgs e)
        {
            ctlClick = cms.SourceControl;            
        }

        private void buttonCopy_Click(object sender, EventArgs e)
        {
            CreateKKData();
        }

        private string findString(string strFind)
        {
            string res = string.Empty;

            foreach(DataGridViewRow r in dgvP.Rows)
            {
                foreach(DataGridViewTextBoxCell c in r.Cells)
                {
                    if(c.Value.ToString().Contains(strFind))
                    {
                        DataGridViewTextBoxCell value = (DataGridViewTextBoxCell)dgvP.Rows[c.RowIndex].Cells[c.ColumnIndex + 1];
                        res = value.Value.ToString();
                        return res;
                    }
                }
            }
            return res;
        }

   

        private bool CreateKKData()
        {
         //   if (findString("記号・番号") != string.Empty) return false;

            try
            {
                DataRow dr = dtKK.Rows[0];
                clsKKData.ManageInfo clsKKMng = new clsKKData.ManageInfo();
                clsKKMng.strKoban = dr["項番"].ToString();

                clsKKMng.strBranchNumber = dgvK.Rows[0].Cells[4].Value.ToString();
                clsKKMng.strRezeptCount = dgvK.Rows[2].Cells[1].Value.ToString();
                clsKKMng.strStartDate = dgvK.Rows[4].Cells[1].Value.ToString();
                clsKKMng.strFinishtDate = dgvK.Rows[4].Cells[2].Value.ToString();
                clsKKMng.strPageForRezept = dgvK.Rows[6].Cells[3].Value.ToString();
                clsKKMng.strCreator = dgvK.Rows[7].Cells[1].Value.ToString();

                clsKKMng.strShibu = "神奈川";// dgvP.Rows[1].Cells[2].Value.ToString();

              
                clsKKMng.strHihoMark = dr["被保険者記号"].ToString();
                clsKKMng.strHihoNum = dr["被保険者番号"].ToString();


                clsKKMng.strHihoName = dgvP.Rows[3].Cells[2].Value.ToString();
                clsKKMng.strPatientName = dgvP.Rows[4].Cells[2].Value.ToString();
                clsKKMng.strPatientBirthday = dgvP.Rows[5].Cells[2].Value.ToString();
                clsKKMng.strShinryoYM = dgvP.Rows[1].Cells[5].Value.ToString();
                clsKKMng.strHosName = dgvP.Rows[5].Cells[4].Value.ToString();
                clsKKMng.strDisease = dgvP.Rows[7].Cells[2].Value.ToString();

                List<clsKKData.DetailData> lstKKDetail = new List<clsKKData.DetailData>();
                
                foreach (DataGridViewRow r in dgv.Rows)
                {
                    clsKKData.DetailData clsDetail = new clsKKData.DetailData();
                    
                    clsDetail.strShinryoBunrui = r.Cells[0].Value.ToString();
                    clsDetail.strKomoku = r.Cells[1].Value.ToString();
                    clsDetail.strScore = r.Cells[2].Value.ToString();
                    clsDetail.strAmount = r.Cells[3].Value.ToString();
                    clsDetail.strScoreSum = r.Cells[4].Value.ToString();
                    clsDetail.strHMS = r.Cells[5].Value.ToString();

                    lstKKDetail.Add(clsDetail);
                    

                }

                clsKKData.Biko clsBiko = new clsKKData.Biko();
                clsBiko.strBiko = txtBiko.Text.Trim();

                frmInput_forexcel frm = new frmInput_forexcel(clsKKMng, lstKKDetail,clsBiko.strBiko);
                frm.Show();
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
    }

  
}

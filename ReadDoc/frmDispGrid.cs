﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Npgsql;

namespace ReadDoc
{
    public partial class frmDispGrid : Form
    {
        private const int baseRowHeight= 24;

        public frmDispGrid(DataSet ds, string strFullPath)
        {
            InitializeComponent();
            disp(ds, strFullPath);
        }

        private void disp(DataSet dsdisp,string strFullPath)
        {

            dgvDataDisplay(dsdisp.Tables["data"]);
            dgvDataPersonal(dsdisp.Tables["personal"]);
            this.txtPath.Text = strFullPath;
        }

        /// <summary>
        /// データ部
        /// </summary>
        /// <param name="dtdisp"></param>
        private void dgvDataDisplay (DataTable dtdisp)
        {
 
            //数字用スタイル
            DataGridViewCellStyle csNum = new DataGridViewCellStyle();
            csNum.Font = new Font("ＭＳ　ゴシック", 14);
            csNum.Alignment = DataGridViewContentAlignment.MiddleRight;
            
            //テキスト用スタイル
            DataGridViewCellStyle csTxt = new DataGridViewCellStyle();
            csTxt.Font = new Font("ＭＳ　ゴシック", 14);
            csTxt.Alignment = DataGridViewContentAlignment.MiddleLeft;
            csTxt.WrapMode = System.Windows.Forms.DataGridViewTriState.True;

            dgv.RowHeadersVisible = false;
            dgv.AllowUserToAddRows = false;            
            dgv.DataSource = dtdisp;            

            dgv.RowTemplate.Height = baseRowHeight;
            dgv.ReadOnly = true;
            dgv.ColumnHeadersDefaultCellStyle.Font = new Font("ＭＳ　ゴシック", 11);


            dgv.Columns[0].DefaultCellStyle = csNum;
            dgv.Columns[1].DefaultCellStyle = csTxt;
            dgv.Columns[2].DefaultCellStyle = csTxt;
            dgv.Columns[3].DefaultCellStyle = csNum;
            dgv.Columns[4].DefaultCellStyle = csNum;
            dgv.Columns[5].DefaultCellStyle = csNum;

            dgv.Columns[0].HeaderText = "行";
            dgv.Columns[1].HeaderText = "点数表分類";
            dgv.Columns[2].HeaderText = "項目";
            dgv.Columns[3].HeaderText = "点数";
            dgv.Columns[4].HeaderText = "数量";
            dgv.Columns[5].HeaderText = "合計";

            dgv.Columns[0].Width = 50;
            dgv.Columns[1].Width = 150;
            dgv.Columns[2].Width = 500;
            dgv.Columns[3].Width = 80;
            dgv.Columns[4].Width = 80;
            dgv.Columns[5].Width = 100;

          
            
        }

        /// <summary>
        /// 個人情報部
        /// </summary>
        /// <param name="dtdisp"></param>
        private void dgvDataPersonal(DataTable dtdisp)
        {
            dgvPersonal.AllowUserToAddRows = false;
            dgvPersonal.AllowUserToOrderColumns = false;
            dgvPersonal.RowHeadersVisible = false;
            dgvPersonal.RowTemplate.Height = 30;
            dgvPersonal.DefaultCellStyle.Font = new Font("ＭＳ　ゴシック", 12);
            dgvPersonal.ReadOnly = false;

            dgvPersonal.Columns.Add("col1",string.Empty);
            dgvPersonal.Columns["col1"].Width = 200;
            dgvPersonal.Columns["col1"].DefaultCellStyle.BackColor = Color.LightGray;

            dgvPersonal.Columns.Add("col2",string.Empty);
            dgvPersonal.Columns["col2"].Width = 700;

            this.Text = dtdisp.Rows[0][1].ToString().Trim();

            for (int r = 0; r < 9; r++)
            {
                dgvPersonal.Rows.Add();
            }

            dgvPersonal.Rows[0].Cells[0].Value = "支部";                              
            dgvPersonal.Rows[1].Cells[0].Value = "被保険者証 記号番号";
            dgvPersonal.Rows[2].Cells[0].Value = "被保険者氏名";
            dgvPersonal.Rows[3].Cells[0].Value = "受療者氏名";
            dgvPersonal.Rows[4].Cells[0].Value = "受療者生年月日";            
            dgvPersonal.Rows[5].Cells[0].Value = "傷病名";
            dgvPersonal.Rows[6].Cells[0].Value = "診療年月";
            dgvPersonal.Rows[7].Cells[0].Value = "医療機関所属国";
            dgvPersonal.Rows[8].Cells[0].Value = "診療実日数";



            dgvPersonal.Rows[0].Cells[1].Value =　dtdisp.Rows[0][0].ToString().Trim();          //0p.sibu, 
            dgvPersonal.Rows[1].Cells[1].Value =  dtdisp.Rows[0][1].ToString().Trim();          //1p.hihonum, 
            dgvPersonal.Rows[2].Cells[1].Value =  dtdisp.Rows[0][2].ToString().Trim();          //2p.hihoname, 
            dgvPersonal.Rows[3].Cells[1].Value =  dtdisp.Rows[0][3].ToString().Trim();          //3p.juryoname, 

            if (DateTime.TryParse(dtdisp.Rows[0][4].ToString(), out DateTime tmp))
            {
                dgvPersonal.Rows[4].Cells[1].Value = tmp.ToString("yyyy/MM/dd");
            }
            else
            {
                dgvPersonal.Rows[4].Cells[1].Value = dtdisp.Rows[0][4].ToString();          //4p.juryobirthday, 
            }
            
            
            dgvPersonal.Rows[5].Cells[1].Value =  dtdisp.Rows[0][7].ToString().Trim();          //5p.ym, 
            dgvPersonal.Rows[6].Cells[1].Value =  dtdisp.Rows[0][5].ToString().Trim();          //6p.country, 
            dgvPersonal.Rows[7].Cells[1].Value =  dtdisp.Rows[0][6].ToString().Trim();          //7p.dname, 
            dgvPersonal.Rows[8].Cells[1].Value = dtdisp.Rows[0][8].ToString().Trim();          //8p.countday, 





        }

    
        /// <summary>
        /// 内容によって高さ変更
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmDispGrid_Shown(object sender, EventArgs e)
        {
            int h = 50;//2行の場合の高さ
            

            foreach(DataGridViewRow r in dgv.Rows)
            {
                int lng = r.Cells[2].Value.ToString().Length;
                r.Height = h*(lng / 30)+ baseRowHeight;
            }
        }


        private Control ctlClick;

        #region コピー文字化け対策
        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Control c = ActiveControl;
            if (ctlClick != null)
            {
                c = ctlClick;
            }


            if (c.GetType().Equals(typeof(DataGridView)))
            {
                DataGridView tmpdgv = new DataGridView();
                tmpdgv = (DataGridView)c;
                Clipboard.SetDataObject(tmpdgv.GetClipboardContent());
                Clipboard.SetDataObject(Clipboard.GetData(DataFormats.Text));
            }

        }



        #endregion

        private void cms_Opening(object sender, CancelEventArgs e)
        {
            ctlClick = cms.SourceControl;
        }

        private void dgvPersonal_KeyDown(object sender, KeyEventArgs e)
        {
            ctlClick = cms.SourceControl;
        }
    }
}
